"use strict";

/* Funções Gerais */

const setBirthAge = function( birthDate ) // Definir Ano Etário
	{ return Math.floor( ( Date.now() - birthDate )/( 1000 * 60 * 60 * 24 * 365.25 ) ) };

Object.defineProperties( HTMLDListElement.prototype,
  { oGetTerms:
      { value: function getTerms() // Retorna todos os DT diretos da DL
          { return Array.from( this.children ).filter( element => element.tagName == "DT" ) } },
    oGetDescriptions:
      { value: function getDescriptions() // Retorna todos os DD diretos da DL
          { return Array.from( this.children ).filter( element => element.tagName == "DD" ) } },
    oGetEntries:
      { value: function getEntries() // Retorna arranjos encabeçados pelos DT diretos da DL e continuados por seus respectivos DD
          { var dt = this.oGetTerms(), dlEntries = [];
            for( let i = 0; i < dt.length; i++ )
              { dlEntries.push( Array.of( dt[i] ) );
                var targetElement = dlEntries[i][0].nextElementSibling;
                while( targetElement && targetElement.tagName != "DT" )
                  { if( targetElement.tagName == "DD" ) dlEntries[i].push( targetElement );
                    targetElement = targetElement.nextElementSibling } };
            return dlEntries } } } );

/* Identificadores Globais */

const staticContent = document.getElementById( "static-content" ); // Seção de Conteúdos Estáticos
staticContent:
	{ let content = staticContent;
		content.topicsList = staticContent.querySelector( "dl" ); // Lista com Assuntos para Exibição
	 	topicsList:
			{ let list = content.topicsList;
			 	list.dt = list.oGetTerms(); list.dd = list.oGetDescriptions(); list.topics = list.oGetEntries(); // Tópicos da Lista de Assuntos
				topics:
					{ let topics = list.topics;
					 	topics.contents = []; for( let dd of list.dd ) topics.contents.push( dd.firstElementChild ); // Conteúdo Interno dos Tópicos
					 	contents:
						{ let contents = topics.contents;
						 	contents.keyAnchors = Array.from( list.getElementsByClassName( "key-anchor" ) ); contents.keyTargets = Array.from( list.getElementsByClassName( "key-target" ) ); // Âncoras para Seções da Lista & Suas Respectivas Seções
							contents.meters = list.getElementsByTagName( "meter" ) } } } }; // Barras de Medida da Lista de Assuntos

const dynamicContent = document.getElementById( "dynamic-content" ); // Seção de Conteúdos Dinâmicos
dynamicContent:
	{ let content = dynamicContent;
	 	content.header = dynamicContent.querySelector( "header" ); // Cabeçalho da Seção Dinâmica Atual
	 	header:
	 		{ let header = content.header;
			 	header.heading = header.querySelector( "h1" ); header.exitImage = header.querySelector( "img" ) } }; // Título e Botão de Saída da Seção Dinâmica

/* Ouvintes de Evento */

windowInstructions: // Instruções para a Janela
	{ window.onload = () => { configExitImage(); printMyAge(); setMetersTitle() }; // Instruções Iniciais
		window.onresize = () => configExitImage() }; // Verificar se o Botão de Saída da Seção Dinâmica pode ser Exibido

topicContentDisplay: // Exibir Conteúdo de Tópicos da Lista de Assuntos
	{ let primaryTopics = staticContent.topicsList.dt;
		for( let topic of primaryTopics )
			{ topic.onclick = event => { displayTopicContent( event ); setScrollPosition( event ) };
				topic.onfocus = event => { hightlightCurrentTopic( event ); displayTopicContent( event ) } } };

/* Funções Geradoras */

const changeClickTarget = function*() // Direcionar o Conteúdo Dinâmico até o Alvo de uma Âncora
	{ setContentAnchors: // Definir as Âncoras Atuais do Conteúdo Dinâmico
			{ let contentAnchors = dynamicContent.getElementsByClassName( "key-anchor" );
				for( let anchor of contentAnchors ) anchor.onclick = event => dynamicContent.target.next( event )	};
		moveToSameContent: // Definição de Alvo em Mesmo Conteúdo do da Âncora para o qual Mover a Barra de Rolagem
			{ while( true )
			 		{ var clickEvent = yield;
						var targetValue = staticContent.topicsList.topics.contents.keyTargets.find( target => target.getAttribute( "data-value" ) == clickEvent.target.textContent ).getAttribute( "data-value" );
						let	contentTarget = dynamicContent.querySelector( `[ data-value='${ targetValue }' ]` );
						if( !contentTarget ) break;
						setScrollPosition( contentTarget ) } };
		moveToDifferentContent: // Definição de Alvo em Conteúdo Diferente do da Âncora para o qual Mover a Barra de Rolagem
			{ dynamicContent.target.ready = true;
				yield window.setTimeout( () => { staticContent.topicsList.topics.find( topic =>
					{ for( let item of topic ) { if( item.querySelector( `[ data-value='${ targetValue }' ]` ) ) return true } } )[0].focus() }, .1 );
				return setScrollPosition( dynamicContent.querySelector( `[ data-value='${ targetValue }' ]` ) ) };
		function setScrollPosition( target ) // Mover a Barra de Rolagem para Alvo
	 		{ if( window.innerWidth > 899 ) dynamicContent.scrollTop = target.offsetTop - dynamicContent.header.offsetHeight
			 	else dynamicContent.parentElement.scrollTop = target.offsetTop - dynamicContent.header.offsetHeight } };

/* Funções Específicas */

const printMyAge = function() // Inseri Idade Atualizada ao Lado da Data de Nascimento
	{ var birthDate = document.querySelector( "[ datetime='1994-08-17T00:00' ]" ),
		 		myAge = setBirthAge( new Date( birthDate.dateTime ) );
		birthDate.insertAdjacentText( "beforeend", ` (${ myAge } anos)` ) };

const setMetersTitle = function() // Defini Texto do Atributo Title de <meter>
	{ let meters = staticContent.topicsList.topics.contents.meters;
	 	for( let meter of meters )
	  	{ let title = document.createAttribute( "title" ), rating = meter.value;
				switch( true )
			 		{ case ( rating < .2 ): title.value = "Muito ruim"; break;
						case ( rating < .4 ): title.value = "Ruim"; break;
						case ( rating < .6 ): title.value = "Razoável"; break;
						case ( rating < .8 ): title.value = "Bom"; break;
						case ( rating < .95 ): title.value = "Muito bom"; break;
						default: title.value = "Ótimo" };
				meter.setAttributeNode( title ) } };

const displayTopicContent = function( event ) // Exibe Conteúdo de Tópicos da Lista de Assuntos
	{ var sectionTitle = dynamicContent.header.heading;
		if( event.currentTarget.textContent == sectionTitle.textContent ) return;
		removeTopicContent: // Remove Conteúdo Exibido Original
			{ while( dynamicContent.header.nextElementSibling ) dynamicContent.removeChild( dynamicContent.header.nextElementSibling ) };
		insertTopicContent: // Inseri Novo Conteúdo para Exibição
			{ var topicSet = staticContent.topicsList.topics.find( topicSet => topicSet.includes( event.currentTarget ) );
				let	fragment = document.createDocumentFragment();
				for( let i = 1; i < topicSet.length; i++ ) fragment.appendChild( topicSet[i].firstElementChild.cloneNode( true ) );
				document.importNode( fragment, true ); dynamicContent.appendChild( fragment ) };
		configTitle: // Configurar Título da Seção Dinâmica
			{ let dt = topicSet[0];
				sectionTitle.innerHTML = dt.innerHTML; sectionTitle.className = dt.classList[0] };
		resolveClickTarget: // Se aplicável, direciona a barra de rolagem para o alvo da âncora que originou o novo conteúdo
			{ dynamicContent.target && dynamicContent.target.ready ? dynamicContent.target.next() : dynamicContent.scrollTop = 0 };
		dynamicContent.target = changeClickTarget(); dynamicContent.target.next() };

const configExitImage = function()
	{ if( window.innerWidth >= 425 ) return;
		let exitImage = dynamicContent.header.exitImage;
	 	exitImage.setAttribute( "src", exitImage.getAttribute( "data-src" ) ); exitImage.removeAttribute( "data-src" );
		exitImage.parentElement.onclick = event => setScrollPosition( event );
		window.onresize = null };

const setScrollPosition = function( event )
	{ if( window.innerWidth >= 425 ) return;
		staticContent.topicsList.dt.some( topic => topic == event.currentTarget ) ? dynamicContent.parentElement.scrollTop = dynamicContent.offsetTop : dynamicContent.parentElement.scrollTop = 0 };

const hightlightCurrentTopic = function( event )
	{ if( event.target.classList.contains( "current-topic" ) ) return;
 		staticContent.topicsList.querySelector( ".current-topic" ).classList.remove( "current-topic" );
		event.target.classList.add( "current-topic" )	};
