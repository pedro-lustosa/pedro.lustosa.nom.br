"use strict";

/* Funções Gerais */

Object.defineProperty( EventTarget.prototype, "oCatchClicksWithKeys",
  { value: function catchClicksWithKeys( event, keys = [ "Enter" ] ) // Acionar Cliques através de Teclagens
      { if( keys.some( key => event.key == key ) ) this.click() } } );

/* Identificadores Globais */

const fixedHeader = document.getElementById( "fixed-header" ); // Cabeçalho Fixo
fixedHeader:
	{ let header = fixedHeader;
		header.banner = document.getElementById( "intro-banner" ); // Banner de Apresentação
		header.displayer = document.querySelector( "[ data-id$='displayer' ]" ); // Barra de Mensagens & Pesquisa
		displayer:
			{ let displayer = header.displayer;
			 	displayer.imageBlock = displayer.querySelector( "#displayer-image" ); // Recipiente da Imagem com a Lupa
				displayer.outputText = displayer.querySelector( "samp" );	displayer.queryInput = displayer.querySelector( "input" ) } // Mensagens & Input de Pesquisa
		header.navMenu = document.getElementById( "fixed-nav" ); // Barra de Navegação
		navMenu:
			{ let menu = header.navMenu;
				menu.lists =
					{ all: Array.from( menu.getElementsByTagName( "ul" ) ),
				 		main: menu.querySelector( "ul" ), side: Array.from( menu.querySelectorAll( "li > ul" ) ) };
				menu.anchors =
					{ all: Array.from( menu.getElementsByTagName( "a" ) ), // Âncoras do Menu de Navegação
						main: Array.from( header.querySelectorAll( "#fixed-nav > ul > li > a" ) ), side: Array.from( menu.querySelectorAll( "li > ul a" ) ) } } }; // Âncoras Iniciais & Colaterais do Menu

const contentScreen = document.querySelector( "iframe[ name='content-screen' ]" ); // Iframe com o Conteúdo Central
contentScreen:
	{ contentScreen.infoBox = document.querySelector( "#frame-container .info-box" ) }; // Faixa Informativa do Iframe

/* Ouvintes de Eventos */

initialInstructions: // Instruções Iniciais
	{ window.onload = () => { changeBaseTarget(); changeHeaderText() } };

configNewWindow: // Alterar Endereço da Janela para a do Iframe
	{	window.onkeydown = event => changeWindowLocation( event, contentScreen.contentWindow.location.href );
		contentScreen.onload = () => configNewWindow( contentScreen.contentWindow.location.href ) };

headerTextChange: // Alterar Mensagem Informativa do Cabeçalho Fixo
	{ for( let anchor of fixedHeader.navMenu.anchors.all )
			{ anchor.onmouseenter = event => changeHeaderText( event ); anchor.onmouseleave = event => changeHeaderText( event );
				anchor.onfocus = event => changeHeaderText( event ); anchor.onblur = event => changeHeaderText( event ) } };

navMenuSlide: // Controle do Deslocamento das Listas do Menu de Navegação
	{ for( let list of fixedHeader.navMenu.lists.side )
			{ list.parentElement.onmouseenter = event => slideNavMenu( event, event.currentTarget ); list.parentElement.onclick = event => slideNavMenu( event, event.currentTarget ) };
		for( let anchor of fixedHeader.navMenu.anchors.side ) anchor.onclick = event => slideNavMenu( event, event.currentTarget ) };

infoBoxAnimation: // Controle do Acionamento da Animação da Faixa Informativa
	{ contentScreen.infoBox.addEventListener( "animationiteration", () => animateInfoBox( false ) ) };

displayerRoleSwitch: // Alterar a Função da Barra de Exibição do Cabeçalho Fixo entre Barra de Mensagens e Barra de Pesquisa
	{ let lupeContainer = fixedHeader.displayer.imageBlock;
		lupeContainer.onclick = () => switchDisplayerRole();
	 	lupeContainer.onkeypress = event => event.currentTarget.oCatchClicksWithKeys( event ) };

/* Funções Específicas */

const changeBaseTarget = function() // Alteração do Alvo Padrão de Contextos de Navegação
	{ if( screen.availHeight < 600 ) document.querySelector( "base" ).target = "_self" };

const configNewWindow = function( currentLocation ) // Determinar Funcionalidades para Novas Janelas
	{ if( !fixedHeader.navMenu.anchors.side.some( anchor => currentLocation == anchor.href ) ) return animateInfoBox( false );
		contentScreen.contentWindow.addEventListener( "keydown", event => changeWindowLocation( event, currentLocation ) );
	 	return animateInfoBox() };

const changeWindowLocation = function( event, targetLocation ) // Alterar Endereço da Janela para a do Iframe
	{	if( !( event.code == "KeyR" && event.ctrlKey && event.altKey ) ) return;
		if( fixedHeader.navMenu.anchors.side.some( anchor => targetLocation == anchor.href ) ) window.location.assign( targetLocation ) };

const changeHeaderText = function( event ) // Alterar Mensagem Informativa do Cabeçalho Fixo
	{	let displayerText = fixedHeader.displayer.outputText;
		if( event && ( event.type == "mouseenter" || event.type == "focus" ) )
			{ displayerText.innerHTML = event.currentTarget.getAttribute( "data-description" ) }
 		else if( fixedHeader.navMenu.contains( document.activeElement ) )
			{ displayerText.innerHTML = document.activeElement.getAttribute( "data-description" ) }
		else if( contentScreen.contentWindow.location.pathname.includes( "sobre-mim" ) )
			{ displayerText.innerHTML = displayerText.getAttribute( "data-description" ) }
		else
			{ displayerText.innerHTML = fixedHeader.navMenu.anchors.all.find( anchor => contentScreen.contentWindow.location.href == anchor.href ).getAttribute( "data-description" ) } };

const slideNavMenu = function( event, target ) // Controle do Deslocamento das Listas do Menu de Navegação
	{ if( target.tagName == "A" )
			{ fixedHeader.navMenu.lists.side.find( list => list.contains( target ) ).style.height = "0"; event.stopPropagation() }
		else if( target.tagName == "LI" )
			{ target.querySelector( "ul" ).removeAttribute( "style" ) } };

const animateInfoBox = function( animate = true ) // Controle do Acionamento da Animação da Faixa Informativa
	{ if( animate && screen.availWidth > 799  ) { contentScreen.infoBox.style.display = "block"; contentScreen.infoBox.style.animationPlayState = "running" }
 		else { contentScreen.infoBox.removeAttribute( "style" ) } };

const switchDisplayerRole = function( event ) // Alterar a Função da Barra de Exibição do Cabeçalho Fixo entre Barra de Mensagens e Barra de Pesquisa
	{ let	displayer = fixedHeader.displayer; var originalId = displayer.id;
	 	displayer.setAttribute( "id", displayer.getAttribute( "data-id" ) ); displayer.setAttribute( "data-id", originalId ) };
