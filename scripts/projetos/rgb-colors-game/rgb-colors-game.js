"use stric";

/* Identificadores Globais */

const gameHeader = document.getElementById( "gameHeader" ); // Cabeçalho do Jogo
gameHeader:
	{ gameHeader.colorText = gameHeader.querySelector( "h1 span" ) }; // Texto com a Cor RGB Alvo

const gameMenu = document.getElementById( "gameMenu" ); // Menu do Jogo
gameMenu:
	{ let menu = gameMenu;
		menu.newGameButton = menu.querySelector( "#menuList > :first-child" ), // Botão de Novo Jogo
  	menu.log = menu.querySelector( "#menuList > li output" ), menu.difficulty = menu.querySelectorAll( "#menuList > ul li" ) }; // Texto de Atualização de Escolha de Quadrado & Lista de Dificuldades

const gameSquares = // Quadrados do Jogo
	{ container: document.getElementById( "squaresContainer" ), all: Array.from( document.getElementsByClassName( "square" ) ) };

const gameColors = new Object(), // Cores do Jogo
			selection = { color: undefined, difficulty: gameMenu.difficulty[1] }; // Seleções

/* Ouvintes de Evento */

initialInstructions: //Instruções Iniciais
	{ window.onload = () => styleMenu()
		if( screen.availWidth >= 450 ) window.addEventListener( "resize", styleMenu ) }; // Complemento Estilístico para o Menu

newGameStart: // Início de (Novo) Jogo
	{ gameMenu.newGameButton.addEventListener( "click", resetGame ) };

setDifficulty: // Definição de Dificuldade
	{ for( let i = 0; i < gameMenu.difficulty.length; i++ )
		{ gameMenu.difficulty[i].addEventListener( "click", resetDifficulty ) } };

bubbleControl: // Remoção de Filtro & Controle de Borbulho
	{ gameSquares.all.forEach( square => square.addEventListener( "transitionend", event =>
		{ event.target.style.filter = "none"; event.stopPropagation() } ) ) };

betweenGames: // Entremeio de Jogos
	{ gameSquares.container.addEventListener( "transitionstart", event =>			// Configuração Durante [ Aparecimento/Desaparecimento ] dos Quadrados
			{ event.target.style.filter = "grayscale(100%)";
		 		for( let square of event.target.children || Array.of( event.target ) ) square.style.cursor = "" } );
		gameSquares.container.addEventListener( "transitionend", setSelection ) }; // Configuração ao Fim do [ Aparecimento/Desaparecimento ] dos Quadrados

/* Funções Específicas */

function resetGame() // Iniciar (Novo) Jogo
	{	if( gameSquares.all[0].onclick )
			{ for( let square of gameSquares.all ) square.onclick = null };
		if( gameMenu.newGameButton.textContent != "New Colors" && gameSquares.container.style.opacity == 0 )
			{ selectColor() }
		else if( gameMenu.newGameButton.textContent != "Start Game" && gameSquares.container.style.opacity == 1 )
			{ gameHeader.style.backgroundColor = ""; gameMenu.style.backgroundColor = "";
				gameHeader.colorText.textContent = "RGB"; gameMenu.log.textContent = "";
				animationFlow( gameSquares.container, "Down" ) };
		setTimeout( () => gameMenu.newGameButton.textContent = "New Colors", 1000 ) };

function animationFlow( element, direction ) // Animação de Opacidade
	{ direction == "Up" ? element.style.opacity = "1" : element.style.opacity = "0" };

function setSelection() // Modificações segundo Opacidade
	{ if( this.style.opacity == 1 )
			{ this.style.filter = "";
				for( let square of this.children )
					{	square.style.cursor = "pointer"; square.onclick = chooseSquare.bind( square, gameSquares.right, gameColors.right ) } }
		else
			{ for( let square of this.children ) square.style = ""; selectColor() } };

function resetDifficulty() // Definição da Dificuldade
	{ if( selection.difficulty != this )
			{ selection.difficulty.classList.remove("selected");
				selection.difficulty = this;
			 	selection.difficulty.classList.add("selected"); styleMenu() } };

function styleMenu() // Complemento Estilístico para o Menu
	{ if( window.innerWidth < 450 )
			{ gameMenu.newGameButton.style.backgroundImage = window.getComputedStyle( selection.difficulty ).backgroundImage;
			 	if( gameMenu.newGameButton.textContent == "Play Again?" ) gameMenu.style.backgroundColor = gameSquares.right.style.backgroundColor }
	 	else
			{ gameMenu.newGameButton.style.backgroundImage = null; gameMenu.style.backgroundColor = null } };

// Definição das Cores
	function selectColor() // Correta
		{	gameColors.wrongs = [];
			gameColors.right = { red: Math.trunc( Math.random() * 256 ), green: Math.trunc( Math.random() * 256 ), blue: Math.trunc( Math.random() * 256 ) };
			selection.color = `(${ gameColors.right.red }, ${ gameColors.right.green }, ${ gameColors.right.blue })`;
			gameSquares.wrongs = Array.from( document.getElementsByClassName( "square" ) );
			gameSquares.right = gameSquares.wrongs.splice( Math.trunc( Math.random() * gameSquares.wrongs.length ), 1 ).pop();
			gameSquares.right.style.backgroundColor = "rgb" + selection.color; gameHeader.colorText.textContent += selection.color;
			pickColors( gameSquares.wrongs, gameColors.right, gameColors.wrongs );
			animationFlow( gameSquares.container, "Up" ) };

	function pickColors( wrongSquares, rightRgb, wrongRgb, squareToRevalue ) // Outras
		{ var sum, totalSum; sum = [];
			if ( squareToRevalue == undefined )
				{ for( let square of wrongSquares ) wrongRgb.push( [] ) };
			for( let i = squareToRevalue || 0; i < wrongSquares.length; i++ )
				{	switch( selection.difficulty.textContent )
						{ case "Easy":
								totalSum = Math.trunc( Math.random() * 26 ) + 185;
								var revalueCeil = 30;
								do
									{ var x = Math.min( ( Math.random() * .201 ), .2 ) + .4;
										var y = Math.min( ( Math.random() * .201 ), .2 ) }
									while( (x - y) < .3 );
								break;
							case "Normal":
								totalSum = Math.trunc( Math.random() * 26 ) + 125;
								var revalueCeil = 20;
								do
									{	var x = Math.min( ( Math.random() * .1501 ), .15 ) + .35;
									 	var y = Math.min( ( Math.random() * .2501 ), .25 ) + .15 }
									while( false );
								break;
							case "Hard":
								totalSum = Math.trunc( Math.random() * 26 ) + 65;
								var revalueCeil = 10;
								do
									{ var x = Math.min( ( Math.random() * .201 ), .2 ) + .2;
							 			var y = Math.min( ( Math.random() * .201 ), .2 ) + .2 }
									while( (x + y) < .6 ) };
					sum[0] = Math.round( totalSum * x ); sum[1] = Math.round( totalSum * y );
					sum[2] = Math.round( totalSum * ( 1 - (x + y) ) );
					for( let n = 0; n != 3; n++ )
						{ wrongRgb[i][n] = Number( sum.splice( Math.trunc( Math.random() * sum.length ), 1 ) ) };
					var operationSetter = Math.random();
				 	if( operationSetter >= .5 )
						{ wrongRgb[i][0] += rightRgb.red; wrongRgb[i][1] += rightRgb.green; wrongRgb[i][2] += rightRgb.blue;
							for( let color of wrongRgb[i] )
								{ if( color > 255 )
									{ let n = color - 255, target = wrongRgb[i].indexOf(color);
										wrongRgb[i][target] = 255 - n } } }
					else
						{ wrongRgb[i][0] = Math.abs( rightRgb.red - wrongRgb[i][0] );
							wrongRgb[i][1] = Math.abs( rightRgb.green - wrongRgb[i][1] );
							wrongRgb[i][2] = Math.abs( rightRgb.blue - wrongRgb[i][2] ) };
					wrongSquares[i].style.backgroundColor = `rgb(${ wrongRgb[i][0] },${ wrongRgb[i][1] },${ wrongRgb[i][2] })`;
					if( squareToRevalue != undefined ) break };
			xLoop: for( x = 0; x < wrongSquares.length; x++ )
				{ yLoop: for( y = x + 1; y < wrongSquares.length; y++ )
					{ if( ( (	Math.abs( wrongRgb[x][0] - rightRgb.red ) ) <= revalueCeil &&
									( Math.abs( wrongRgb[x][1] - rightRgb.green ) ) <= revalueCeil &&
									( Math.abs( wrongRgb[x][2] - rightRgb.blue ) ) <= revalueCeil )
								||
								( (	Math.abs( wrongRgb[x][0] - wrongRgb[y][0] ) ) <= revalueCeil &&
									( Math.abs( wrongRgb[x][1] - wrongRgb[y][1] ) ) <= revalueCeil &&
									( Math.abs( wrongRgb[x][2] - wrongRgb[y][2] ) ) <= revalueCeil ) )
								{ pickColors( wrongSquares, rightRgb, wrongRgb, x );
									break xLoop } } } };

function chooseSquare( rightSquare, rightRgb ) // Seleção dos Quadrados
	{	if( this != rightSquare )
			{ this.onclick = null; this.style.opacity = "0"; this.style.cursor = "";
				gameMenu.log.textContent = "Try Again" }
 		else
			{ for( let square of gameSquares.all )
					{ square.onclick = null;
						square.style = "background-color: rgb(" + rightRgb.red + "," + rightRgb.green + "," + rightRgb.blue + "); opacity: 1; cursor: pointer" };
				gameHeader.style.backgroundColor = "rgb(" + rightRgb.red + "," + rightRgb.green + "," + rightRgb.blue + ")";
				if( window.innerWidth < 450 ) gameMenu.style.backgroundColor =  "rgb(" + rightRgb.red + "," + rightRgb.green + "," + rightRgb.blue + ")";
				gameMenu.log.textContent = "Correct!"; gameMenu.newGameButton.textContent = "Play Again?" } };
