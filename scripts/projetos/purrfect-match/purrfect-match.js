"use strict";
/* Identificadores Globais */

const topHeader = // Menus Superiores
	{ staticMenu: document.getElementById( "staticMenu" ), slidingMenu: null, // Menu Estático e Menu Deslizante
 		pagesList: document.getElementById( "pagesList" ), loginList: document.getElementById( "loginList" ) }; // Lista de Páginas e Lista de Login
	staticMenu:
		{ let menu = topHeader.staticMenu;
			menu.left = document.getElementById( "staticMenuLeft" ); menu.right = document.getElementById( "staticMenuRight" ) };

/* Ouvintes de Evento */

slidingMenuToggle: // Alternação de Inclusão do Menu Deslizante
	{ window.onresize = () => toggleSlidingMenu() };

slidingMenuHide: // Esconder Menu Deslizante
	{ for( let list of topHeader.pagesList.children ) list.onclick = () => hideSlidingMenu();
	 	for( let list of topHeader.loginList.children )	list.onclick = () => hideSlidingMenu() };

/* Funções Específicas */

const toggleSlidingMenu = function() // Alteração do Menu Superior
	{ var staticMenu = topHeader.staticMenu;
		if( window.innerWidth < 650 && !topHeader.slidingMenu )
	 		{	createMenu: // Criar Menu Deslizante
					{ var slidingMenu = topHeader.slidingMenu = document.createElement( "NAV" ); slidingMenu.setAttribute( "id", "slidingMenu" );
						document.body.insertBefore( slidingMenu, staticMenu.nextElementSibling ) };
				createMenuButton: // Criação do Botão do Menu Deslizante
					{ slidingMenu.button = document.createElement( "BUTTON" ); slidingMenu.button.setAttribute( "id", "slidingButton" );
						staticMenu.right.appendChild( slidingMenu.button ) };
				fillMenu: // Adição de Conteúdo ao Menu Deslizante
					{ slidingMenu.appendChild( topHeader.pagesList ); slidingMenu.insertBefore( topHeader.loginList, topHeader.pagesList ) };
				moveMenu: // Deslocamento do Menu Superior
					{ slidingMenu.button.onclick = () => slidingMenu.classList.toggle( "moved" ) } }
		else if( window.innerWidth >= 650 && topHeader.slidingMenu )
	 		{ var slidingMenu = topHeader.slidingMenu;
				changePlacement: // Alteração da Localização do Conteúdo Estático do Menu
					{	staticMenu.left.appendChild( topHeader.pagesList ); staticMenu.right.appendChild( topHeader.loginList ) };
				removeMenu: // Retirada do Menu Deslizante
					{ slidingMenu.button.remove(); slidingMenu.remove();
			 			topHeader.slidingMenu = null } } };

const hideSlidingMenu = function()
	{ if( topHeader.slidingMenu ) topHeader.slidingMenu.classList.remove( "moved" ) };

/* Instruções Iniciais */

toggleSlidingMenu();
