"use strict";

/* Funções Gerais */

Object.defineProperty( Array.prototype, "oSegmentByEnd",
  { value: function segmentByEnd( evaluer, trim = false ) // Divide o arranjo em sub-arranjos, a terminarem com o valor que atender à avaliação da função de parâmetro
      { var newArray = [], startIndex = 0;
        this.forEach( ( value, index, array ) =>
          { if( evaluer( value ) ) { newArray.push( array.slice( startIndex, index + 1 ) ); startIndex = index + 1 } } );
        if( !trim && startIndex != this.length ) newArray.push( this.slice( startIndex ) );
        return newArray } } );

Object.defineProperty( HTMLElement.prototype, "oBringIt", // Retorna um Elemento Descendente de Outro
  { value: function bringIt( objectList, objectPlacement )
      { var targetObjects, targetObjects = [];
        for( let object of objectList )
          { if( this.contains( object ) ) targetObjects.push( object ) };
        return ( objectPlacement ? targetObjects[ objectPlacement - 1 ] : targetObjects[0] ) } } );

/* Identificadores Globais */

const taskList = { whole: document.getElementById( "taskList" ) }; // Lista de Tarefas
  taskList:
    { let list = taskList; // Título, Cabeçalho, Corpo e Rodapé da Lista de Tarefas
      list.caption = list.whole.querySelector( "caption" ), list.head = list.whole.querySelector( "thead" ),
      list.body = list.whole.querySelector( "tbody" ), list.foot = list.whole.querySelector( "tfoot" );
      caption:
        { list.caption.inputDisplayer = document.getElementById( "inputDisplayer" ) }; // Exibidor do Campo de Inserção de Itens na Lista
  		head:
        { list.head.entryInput = list.whole.querySelector( "input[ name='entryInput' ]" ) }; // Campo de Inserção de Itens na Lista
  		body:
        { let body = list.body; // Modelo, Fileira, Rótulos, Inputs, Boxes e Textos do Corpo da Lista
          body.template = document.getElementById( "tbodyRow" ), body.tr = Array.from( list.whole.querySelectorAll( "tbody tr" ) ),
  				body.labels = Array.from( list.whole.querySelectorAll( "tbody td > label" ) ), body.inputs = Array.from( list.whole.querySelectorAll( "tbody td > label > input" ) ),
  				body.boxes = Array.from( list.whole.querySelectorAll( "tbody td > label > span:first-of-type" ) ), body.texts = Array.from( list.whole.querySelectorAll( "tbody td > label > span:last-of-type" ) ) };
  		foot:
        { let foot = list.foot; // Funcionalidades no Rodapé da Lista & Barra de Mensagens
          foot.options = Array.from( list.whole.querySelectorAll( "tfoot tr:first-child td" ) ), foot.optionsInfo = list.whole.querySelector( "tfoot tr:last-child samp" ) } };

const inputSelection = taskList.body.inputs.filter( input => input.checked ); // Opções Marcadas da Lista

const contextMenu = document.getElementById( "contextMenu" ); // Menu de Contexto
  contextMenu:
    { let menu = contextMenu; // Lista Principal & Listas Colaterais do Menu de Contexto
      menu.lists = { main: Array.from( document.querySelectorAll( "#contextMenu > ul > li" ) ), side: Array.from( menu.querySelectorAll( "ul ul li" ) ) };
      menu.lists.side = menu.lists.side.oSegmentByEnd( listItem => !listItem.nextElementSibling ) };

/* Ouvintes de Evento */

windowInstructions: // Instruções para a Janela
	{ window.onload = () => { displayInfo(); configTaskItems( "tr", "inputs" ); setVerticalLayout() }; // Instruções Iniciais
		window.onresize = () => setVerticalLayout() } // Ajustar Posição Vertical da Tabela

toggleInputDisplayer: // Revelar & Ocultar da Lista Célula de Inserção
  { let displayer = taskList.caption.inputDisplayer;
  	displayer.onclick = event => inputDisplay( event.currentTarget );
  	displayer.addEventListener( "keypress", event => { if( event.key != "Enter" ) event.stopImmediatePropagation() } );
  	displayer.onkeypress = event => inputDisplay( event.currentTarget ) };

addTask: // Adicionar Novas Tarefas à Lista
  { taskList.head.entryInput.onkeypress = event => addTask( event ) };

adjustListScroll: // Ajustar Comportamento da Barra de Rolagem da Lista
  { taskList.body.onkeydown = event => limitListScroll( event ) };

blurByMouse: // Remover Foco em Função da Distância do Mouse
  { taskList.caption.inputDisplayer.onmouseleave = event => { if( event.target == document.activeElement ) document.activeElement.blur() };
    taskList.foot.onmouseleave = event => { if( event.target.contains( document.activeElement ) ) document.activeElement.blur() } };

listFootFeatures: // Funcionalidades do Menu de Rodapé
  { taskList.foot.addEventListener( "keypress", event => { if( event.key != "Enter" ) event.stopPropagation() }, true );
	  let options = taskList.foot.options;

  	// Indicar Função das Tarefas
  	for( let option of options )
  		{ option.onmouseenter = event => displayInfo( event ); option.onmouseleave = event => displayInfo( event ) };
  	let actualOption;

  	// Atualizar Completude da(s) Tarefa(s)
  	actualOption = options.find( option => option.firstElementChild.getAttribute( "alt" ) == "Concluir" );
  	actualOption.onclick = event => updateTaskCompletion(); actualOption.onkeypress = event => updateTaskCompletion();

  	// Copiar Tarefa(s)
  	actualOption = options.find( option => option.firstElementChild.getAttribute( "alt" ) == "Copiar" );
  	actualOption.onclick = event => copyTask(); actualOption.onkeypress = event => copyTask();

  	// Alternar Importância da(s) Tarefa(s)
  	actualOption = options.find( option => option.firstElementChild.getAttribute( "alt" ) == "Relevar" );
  	actualOption.onclick = event => changeImportance( event ); actualOption.onkeypress = event => changeImportance( event );

  	// Agrupar Tarefas por Importância
  	actualOption = options.find( option => option.firstElementChild.getAttribute( "alt" ) == "Ordenar" );
  	actualOption.onclick = event => sortByImportance(); actualOption.onkeypress = event => sortByImportance();

  	// Colocar Tarefa(s) no Topo da Lista
  	actualOption = options.find( option => option.firstElementChild.getAttribute( "alt" ) == "Alçar" );
  	actualOption.onclick = event => changeTaskPlacement( "aboveAll" ); actualOption.onkeypress = event => changeTaskPlacement( "aboveAll" );

  	// Colocar Tarefa(s) Acima de Uma
  	actualOption = options.find( option => option.firstElementChild.getAttribute( "alt" ) == "Subir" );
  	actualOption.onclick = event => changeTaskPlacement( "aboveOne" ); actualOption.onkeypress = event => changeTaskPlacement( "aboveOne" );

  	// Colocar Tarefa(s) Ao Fim da Lista
  	actualOption = options.find( option => option.firstElementChild.getAttribute( "alt" ) == "Rebaixar" );
  	actualOption.onclick = event => changeTaskPlacement( "beneathAll" ); actualOption.onkeypress = event => changeTaskPlacement( "beneathAll" );

  	// Colocar Tarefa(s) Abaixo de Uma
  	actualOption = options.find( option => option.firstElementChild.getAttribute( "alt" ) == "Descer" );
  	actualOption.onclick = event => changeTaskPlacement( "beneathOne" ); actualOption.onkeypress = event => changeTaskPlacement( "beneathOne" );

  	// Excluir Tarefa(s)
  	actualOption = options.find( option => option.firstElementChild.getAttribute( "alt" ) == "Remover" );
  	actualOption.onclick = event => deleteTask( inputSelection ); actualOption.onkeypress = event => deleteTask( inputSelection ) };

toggleContextMenu: // Alternar Exibição do Menu de Contexto e de seus Menus Colaterais
  { document.oncontextmenu = event => toggleContextMenu( event );
    document.onmousedown = event => toggleContextMenu( event ); contextMenu.onmousedown = event => event.stopPropagation();
    for( let listItem of contextMenu.lists.main )
      { listItem.onmouseenter = event => toggleInnerContextMenus( event ); listItem.onmouseleave = event => toggleInnerContextMenus( event ) } };

contextMenuFeatures: // Funcionalidades do Menu de Contexto & Teclas de Atalho
	{ shortKeys: // Teclas de Atalho
	   { document.body.onkeydown = event => setDocumentKeys( event ) };
	  contextMenu: // Menu de Contexto
	   { let lists = contextMenu.lists.side;

  		// Opções Relativas à Captura
  			// Imprimir Página
  			lists[0].find( item => item.textContent.includes( "Ctrl + P" ) ).onclick = event => window.print();

        // Copiar Tarefa(s)
  			lists[0].find( item => item.textContent.includes( "Shift + C" ) ).onclick = event => copyTask();

      // Opções Relativas à Liberação
        // Recarregar Página
        lists[1].find( item => item.textContent.includes( "Ctrl + R" ) ).onclick = event => window.location.reload();

        // Excluir Tarefa(s)
  			lists[1].find( item => item.textContent.includes( "Shift + Del" ) ).onclick = event => deleteTask( inputSelection );

  		// Opções Relativas à Modificação
  			// Atualizar Completude da(s) Tarefa(s)
  			lists[2].find( item => item.textContent.includes( "Enter" ) ).onclick = event => updateTaskCompletion();

  			// Aumentar Importância da(s) Tarefa(s)
  			lists[2].find( item => item.textContent.includes( "[" ) && !item.textContent.includes( "Alt" ) ).onclick = event => changeImportance( event, "[" );

  			// Diminuir Importância da(s) Tarefa(s)
  			lists[2].find( item => item.textContent.includes( "]" ) && !item.textContent.includes( "Alt" ) ).onclick = event => changeImportance( event, "]" );

  		// Opções Relativas à Organização
        // Ordenar Tarefa(s) Crescentemente por Importância
        lists[3].find( item => item.textContent.includes( "[" ) && item.textContent.includes( "Alt" ) ).onclick = event => sortByImportance( "firstGreaters" );

        // Colocar Tarefa(s) no Topo da Lista
  			lists[3].find( item => item.textContent.includes( "Up" ) && item.textContent.includes( "Shift" ) ).onclick = event => changeTaskPlacement( "aboveAll" );

  			// Colocar Tarefa(s) Acima de Uma
  			lists[3].find( item => item.textContent.includes( "Up" ) && !item.textContent.includes( "Shift" ) ).onclick = event => changeTaskPlacement( "aboveOne" );

        // Colocar Tarefa(s) Abaixo de Uma
  			lists[3].find( item => item.textContent.includes( "Down" ) && !item.textContent.includes( "Shift" ) ).onclick = event => changeTaskPlacement( "beneathOne" );

        // Colocar Tarefa(s) Ao Fim da Lista
  			lists[3].find( item => item.textContent.includes( "Down" ) && item.textContent.includes( "Shift" ) ).onclick = event => changeTaskPlacement( "beneathAll" );

        // Ordenar Tarefa(s) Decrescentemente por Importância
        lists[3].find( item => item.textContent.includes( "]" ) && item.textContent.includes( "Alt" ) ).onclick = event => sortByImportance( "firstLessers" );

  		// Ocultar o Menu de Contexto Após Clique em Seu Item
  		for( let listSet of lists ) listSet.forEach( listItem => listItem.addEventListener( "click", event => toggleContextMenu( event ) ) ) } };

/* Funções Específicas */

const setVerticalLayout = function( originalHeight = taskList.body.scrollHeight, decreasedHeight = 0 ) // Ajustar Posição Vertical da Tabela
	{ var container = taskList.whole.parentElement,
        listBody = taskList.body, listHeight = 0;
    if( decreasedHeight > ( originalHeight - listBody.offsetHeight ) )
      { listBody.style.height = originalHeight - decreasedHeight + "px" };
		for( let component of taskList.whole.children ) listHeight += component.offsetHeight;
    if( ( decreasedHeight && window.innerHeight - listBody.offsetHeight < listHeight - listBody.offsetHeight ) ||
        ( !decreasedHeight && ( window.innerHeight < listHeight || originalHeight != listBody.offsetHeight ) && window.innerHeight * .3 < listHeight - listBody.offsetHeight ) )
			{ listBody.style.height = Math.min( window.innerHeight * .7, window.innerHeight - ( listHeight - listBody.offsetHeight ) ) + "px"; container.style.alignItems = "flex-start" }
		else
			{ listBody.removeAttribute( "style" ); container.removeAttribute( "style" ) } };

const setDocumentKeys = function( event ) // Comandos do Teclado para o Documento
	{	switch ( event.key )
			{	case "R": if( event.ctrlKey ) window.location.reload();
					break;
				case "P": if( event.ctrlKey ) window.print();
					break;
			 	case "Enter": if( event.ctrlKey && event.shiftKey ) { updateTaskCompletion(); event.preventDefault() };
					break;
				case "{":
				case "[":
					if( event.ctrlKey && event.shiftKey && !event.altKey ) { changeImportance( event, "[" ); event.preventDefault() }
					else if( event.ctrlKey && event.shiftKey && event.altKey ) { sortByImportance( "firstGreaters" ); event.preventDefault() };
					break;
				case "}":
				case "]":
					if( event.ctrlKey && event.shiftKey && !event.altKey ) { changeImportance( event, "]" ); event.preventDefault() }
					else if( event.ctrlKey && event.shiftKey && event.altKey ) { sortByImportance( "firstLessers" ); event.preventDefault() };
					break;
				case "Up":
				case "ArrowUp":
					if( !event.shiftKey && event.altKey ) { changeTaskPlacement( "aboveOne" ); event.preventDefault() }
					else if( !event.ctrlKey && event.altKey ) { changeTaskPlacement( "aboveAll" ); event.preventDefault() };
					break;
				case "Down":
				case "ArrowDown":
					if( !event.shiftKey && event.altKey ) { changeTaskPlacement( "beneathOne" ); event.preventDefault() }
					else if( !event.ctrlKey && event.altKey ) { changeTaskPlacement( "beneathAll" ); event.preventDefault() };
					break;
				case "C": if( event.ctrlKey && event.shiftKey ) { copyTask(); event.preventDefault() };
					break;
				case "Del":
				case "Delete": if( event.ctrlKey && event.shiftKey ) { deleteTask( inputSelection ); event.preventDefault() } } };

const toggleContextMenu = function( event ) // Alternar Exibição do Menu de Contexto
	{ var menu = contextMenu;
		return ( event.type == "contextmenu" ? showContextMenu() : hideContextMenu() );
		function showContextMenu()
			{ if( menu.hasAttribute( "hidden" ) ) menu.removeAttribute( "hidden" );
				menu.style.left = ( menu.offsetWidth < ( window.innerWidth - event.clientX ) ? ( event.clientX + "px" ) : ( window.innerWidth - menu.offsetWidth + "px" ) );
				menu.style.top = ( menu.offsetHeight < ( window.innerHeight - event.clientY ) ? ( event.clientY + "px" ) : ( window.innerHeight - ( menu.offsetHeight + ( window.innerHeight - event.clientY ) ) + "px" ) );
	 			event.preventDefault() };
		function hideContextMenu()
			{ if( !menu.hasAttribute( "hidden" ) ) menu.setAttribute( "hidden", "" ) } };

const toggleInnerContextMenus = function( event ) // Alternar Exibição dos Menus Colaterais do Menu de Contexto
  { var menuParent = event.currentTarget, menuParentPosition = menuParent.getBoundingClientRect(),
        menu = contextMenu.lists.side.find( listSet => menuParent.contains( listSet[0] ) )[0].parentElement;
    return ( event.type == "mouseenter" ? showContextMenu() : hideContextMenu() );
    function showContextMenu()
      { if( !menu.style.display ) menu.style.display = "block";
        if( menu.offsetWidth > ( window.innerWidth - menuParentPosition.right ) )
          { menu.style.left = ( ( menu.offsetWidth + Number( window.getComputedStyle( menuParent ).paddingLeft.replace( /px/, "" ) ) ) * -1 ) + "px" };
        if( menu.offsetHeight > ( window.innerHeight - menuParentPosition.bottom ) )
          { menu.style.top = ( ( menu.offsetHeight - ( menuParent.offsetHeight - Number( window.getComputedStyle( menuParent ).paddingBottom.replace( /px/, "" ) ) ) ) * -1 ) + "px" } };
    function hideContextMenu()
      { if( menu.style.display ) menu.removeAttribute( "style" ) } };

const configTaskItems = function() // Verificar e Restaurar Atributos de Elementos das Tarefas
	{	for( let argument of arguments )
			{ for( let component of taskList.body[ argument ] )
				{ for( let attribute of taskList.body.template.content.querySelector( component.tagName ).attributes )
					{ if( component.getAttribute( attribute.name ) != attribute.value ) component.setAttribute( attribute.name, attribute.value ) } } };
		for( let box of taskList.body.boxes )
			{ if( box.nextElementSibling.classList.contains( "finished" ) ) continue;
		 		for( let i = 1; i != 5; i++ )
					{ if( box.classList.contains( "imp" + i ) ) break; if( i != 4 ) continue;
						box.classList.add( "imp2" ) } }
		return configTaskEvents() };

const configTaskEvents = function( newRow, newInput, newText ) // Atribuir Eventos a Itens da Lista de Tarefas
	{ for( let i = 0; i < arguments.length; i++ )
			{ if( !Array.isArray( arguments[i] ) ) arguments[i] = Array.of( arguments[i] ) };
		for( let row of arguments[0] || taskList.body.tr )
			{ row.onkeydown = event =>
					setRowKeys( event, event.currentTarget, taskList.body.labels[ taskList.body.tr.indexOf( event.currentTarget ) ],
											taskList.body.inputs[ taskList.body.tr.indexOf( event.currentTarget ) ], taskList.body.boxes[ taskList.body.tr.indexOf( event.currentTarget ) ],
											taskList.body.texts[ taskList.body.tr.indexOf( event.currentTarget ) ] ) };
		for( let input of arguments[1] || taskList.body.inputs )
			{ input.onclick = event =>
					{ updateSelection( event.target ); taskList.body.tr[ taskList.body.inputs.indexOf( event.target ) ].focus() } };
		for( let text of arguments[2] || taskList.body.texts )
	 		{ text.ondblclick = event =>
				editTaskText( event.target.parentElement, event.target ) } };

const inputDisplay = function( icon ) // Revelar & Ocultar da Lista Célula de Inserção
	{ icon.classList.contains( "plus" ) ? taskList.head.style.visibility = "visible" : taskList.head.style.visibility = "collapse";
		icon.classList.toggle( "plus" ) ? icon.classList.remove( "minus" ) : icon.classList.add( "minus" );
		setVerticalLayout() };

const limitListScroll = function( event ) // Ajustar Comportamento da Barra de Rolagem da Lista
	{ if( event.key == "Up" || event.key == "ArrowUp" || event.key == "Down" || event.key == "ArrowDown" ) event.preventDefault() };

const addTask = function( event ) // Adicionar Novas Tarefas à Lista
	{	if( taskList.head.entryInput.value !== "" && event.key == "Enter" )
			{ var tbody = taskList.body, newCell = tbody.template.content.cloneNode( true ),
						newRow = newCell.querySelector( "tr" ), newInput = newCell.querySelector( "input" ),
						newBox = newCell.querySelector( "span:first-of-type" ), newText = newCell.querySelector( "span:last-of-type" );
				newText.textContent = taskList.head.entryInput.value; taskList.head.entryInput.value = "";
				document.importNode( newCell, true ); configTaskEvents( newRow, newInput, newText );
        tbody.appendChild( newCell ); setVerticalLayout();
				tbody.tr.push( newRow ); tbody.labels.push( newInput.parentElement ); tbody.inputs.push( newInput ); tbody.boxes.push( newBox ); tbody.texts.push( newText ) } };

const removeTask = function( row, label, input, box, text ) // Remover Tarefas da Lista
	{ taskList.body.texts.splice( taskList.body.texts.indexOf( text ), 1 ); taskList.body.boxes.splice( taskList.body.boxes.indexOf( box ), 1 );
		taskList.body.inputs.splice( taskList.body.inputs.indexOf( input ), 1 ); taskList.body.labels.splice( taskList.body.labels.indexOf( label ), 1 );
		taskList.body.tr.splice( taskList.body.tr.indexOf( row ), 1 ); taskList.body.removeChild( row );
		updateSelection( input ) };

const setRowKeys = function( event, targetRow, targetLabel, targetInput, targetBox, targetText ) // Comandos do Teclado ao Focar Item da Lista
	{	switch ( event.key )
			{ case "Enter":
					if( !event.ctrlKey )
						{ targetInput.checked ? targetInput.checked = false : targetInput.checked = true;
							updateSelection( targetInput );
							changeRowSelection( event, "down", targetRow ) }
					else if( !event.shiftKey )
						{ targetText.classList.toggle( "finished" );
					 		resetImportance( Array.of( targetText ) )	}
					event.preventDefault(); break;
				case "Backspace":
					changeRowSelection( event, "up", targetRow );
					if( targetRow.previousElementSibling )
						{ let actualInput = document.activeElement.oBringIt( taskList.body.inputs );
							actualInput.checked ? actualInput.checked = false : actualInput.checked = true;
						 	updateSelection( actualInput ) };
					event.preventDefault(); break;
				case "Insert":
					editTaskText( targetLabel, targetText );
					event.preventDefault(); break;
				case "{":
				case "[":
					if( !event.altKey && ( !event.ctrlKey || !event.shiftKey ) ) { changeImportance( event, event.key, Array.of( targetBox ) ); event.preventDefault() }
					break;
				case "}":
				case "]":
					if( !event.altKey && ( !event.ctrlKey || !event.shiftKey ) ) { changeImportance( event, event.key, Array.of( targetBox ) ); event.preventDefault() }
					break;
				case "Del":
				case "Delete":
					if( event.ctrlKey && event.shiftKey ) return;
					let delConfirm = window.confirm( "Deseja realmente excluir a tarefa em questão?" ); if( !delConfirm ) return;
					targetRow.nextElementSibling ? changeRowSelection( event, "down", targetRow ) : changeRowSelection( event, "up", targetRow );
          let originalHeight = taskList.body.scrollHeight, decreasedHeight = targetRow.offsetHeight;
				  removeTask( targetRow, targetLabel, targetInput, targetBox, targetText ); setVerticalLayout( originalHeight, decreasedHeight );
					event.preventDefault(); break;
				case "Home": focusTheExtremity( true );
					event.preventDefault(); break;
				case "End": focusTheExtremity( false );
					event.preventDefault(); break;
				case "Up":
				case "ArrowUp":
					if( !event.shiftKey && !event.altKey )
						{ changeRowSelection( event, "up", targetRow ) }
					else if( !event.altKey )
						{ changeRowSelection( event, "up", targetRow, 2 ) }
					else if( event.ctrlKey && event.shiftKey && event.altKey )
						{ taskList.body.insertBefore( targetRow, taskList.body.firstElementChild ); targetRow.focus() };
					break;
				case "Down":
				case "ArrowDown":
					if( !event.shiftKey && !event.altKey )
						{ changeRowSelection( event, "down", targetRow ) }
					else if( !event.altKey )
						{ changeRowSelection( event, "down", targetRow, 2 ) }
					else if( event.ctrlKey && event.shiftKey && event.altKey )
						{ taskList.body.appendChild( targetRow ); targetRow.focus() } } };

const displayInfo = function( event ) // Indicar Função das Tarefas
	{ return ( ( event && event.type == "mouseenter" ) ? showInfo() : resetInfo() );
 		function showInfo()
	 		{ taskList.foot.optionsInfo.textContent = event.target.firstElementChild.getAttribute( "data-title" ) };
		function resetInfo()
	 		{ taskList.foot.optionsInfo.textContent = taskList.foot.optionsInfo.getAttribute( "data-description" ) } };

const alertSelectionNeed = function() // Avisar sobre Necessidade de se Criar Seleção
	{ window.alert( "É necessário criar uma seleção para realizar esta operação." ) };


const updateSelection = function( input ) // Atualizar Seleção da Lista
	{ if( taskList.body.inputs.includes( input ) && input.checked ) inputSelection.push( input )
		else if( inputSelection.includes( input ) ) inputSelection.splice( inputSelection.indexOf( input ), 1 ) };

const sortSelection = function( selectionType = "inputs" ) // Linearizar Seleção da Lista
	{ var actualInputs = Array.from( taskList.whole.querySelectorAll( "tbody td > label > input" ) );
		var checkedInputs = actualInputs.filter( input => input.checked );
		if( selectionType == "inputs" ) return checkedInputs;
		let selection = [];
		for( let input of checkedInputs ) selection.push( taskList.body[ selectionType ][ taskList.body.inputs.indexOf( input ) ] );
	 	return selection };

const changeRowSelection = function( event, direction, initialRow, rowsToPass = 0 ) // Alterar Seleção entre Fileiras
	{ var actualRow = initialRow;
		for( var i = -1; i < rowsToPass; i++ )
			{ switch ( direction )
					{ case "up": actualRow.previousElementSibling ? actualRow = actualRow.previousElementSibling : i = rowsToPass;
							break;
						case "down": actualRow.nextElementSibling ? actualRow = actualRow.nextElementSibling : i = rowsToPass } };
		if( ( event.key == "Up" || event.key == "ArrowUp" || event.key == "Down" || event.key == "ArrowDown" ) && event.ctrlKey ) return changeRowPlacement();
		actualRow.focus();
		function changeRowPlacement()
	 		{ if( direction == "up" ) taskList.body.insertBefore( initialRow, actualRow )
		 		else if ( direction == "down" ) taskList.body.insertBefore( initialRow, actualRow.nextElementSibling );
			 	initialRow.focus() } };

const changeTaskPlacement = function( direction ) // Deslocamento de Tarefa(s)
	{ if( !inputSelection.length ) return alertSelectionNeed();
		var targetRows = sortSelection( "tr" );
		switch ( direction )
			{ case "aboveAll": targetRows.reverse(); return throughAll( true );
				case "aboveOne": return throughOne( true );
				case "beneathAll": return throughAll( false );
				case "beneathOne": targetRows.reverse(); return throughOne( false ) };
		function throughAll( toTop )
			{ for( let row of targetRows ) toTop ? taskList.body.insertBefore( row, taskList.body.firstElementChild ) : taskList.body.appendChild( row ) }
		function throughOne( toTop )
			{ for( let row of targetRows )
					{ if( toTop && row.previousElementSibling && !targetRows.includes( row.previousElementSibling ) )
							{ taskList.body.insertBefore( row, row.previousElementSibling ) }
						else if( !toTop && row.nextElementSibling && !targetRows.includes( row.nextElementSibling ) )
							{ taskList.body.insertBefore( row, row.nextElementSibling.nextElementSibling ) } } } };

const focusTheExtremity = function( top ) // Focar Fileiras das Extremidades da Lista
	{ return ( top ? focusTheFirst() : focusTheLast() )
	 	function focusTheFirst()
	 		{ taskList.body.firstElementChild.focus() }
		function focusTheLast()
	 		{ taskList.body.lastElementChild.focus() } };

const updateTaskCompletion = function( selectionList = [] ) // Atualizar Completude da(s) Tarefa(s)
	{ if( !inputSelection.length ) return alertSelectionNeed();
		if( !selectionList.length ) for( let selection of inputSelection ) selectionList.push( selection.nextElementSibling.nextElementSibling );
		let finished = selectionList.filter( selection => selection.classList.contains( "finished" ) );
 		let unfinished = selectionList.filter( selection => !selection.classList.contains( "finished" ) );
		if( finished.length <= unfinished.length )
	 		{ for( let selection of selectionList ) if( !( selection.classList.contains( "finished" ) ) ) selection.classList.add( "finished" ) }
		else
			{ for( let selection of selectionList ) if( selection.classList.contains( "finished" ) ) selection.classList.remove( "finished" ) };
		resetImportance( selectionList ) };

const editTaskText = function( label, text ) // Editar Tarefa
	{ if( text.classList.contains( "finished" ) ) return alert( "Para editar a tarefa, é necessário desmarcar sua conclusão." );
		label.onclick = event => event.preventDefault();
		text.style.userSelect = "text";
		text.contentEditable = true; text.focus();
		text.oncontextmenu = event => event.stopPropagation();
		text.onkeydown = event => { if( event.key == "Enter" ) exitEditArea(); event.stopPropagation() };
		document.body.onclick = event => event.target == text ? text.focus() : exitEditArea();
		function exitEditArea()
			{ text.contentEditable = false; text.removeAttribute( "style" );
				document.body.onclick = label.onclick = text.onkeydown = text.oncontextmenu = null } }


const copyTask = function( selectionList = "" ) // Copiar Tarefa(s)
	{ if( !inputSelection.length ) return alertSelectionNeed();
		var linearSelection = sortSelection( "texts" );
		for( let i = 0; i < linearSelection.length; i++ ) selectionList += ( i + 1 ) + ". " + linearSelection[i].textContent + "\n".repeat( 2 );
		var clipboard = document.body.appendChild( document.createElement("PRE") );
		clipboard.appendChild( document.createTextNode( selectionList ) );
		var copyRange = document.createRange(); copyRange.selectNodeContents( clipboard );
		var actualSelection = window.getSelection(); actualSelection.removeAllRanges();
		actualSelection.addRange( copyRange ); document.execCommand( "copy" );
		actualSelection.removeAllRanges(); document.body.removeChild( clipboard ) };

const deleteTask = function( selectionList ) // Excluir Tarefa(s)
	{ if( !inputSelection.length ) return alertSelectionNeed();
    let delConfirm = window.confirm( "Deseja realmente excluir a seleção de tarefas?" ); if( !delConfirm ) return;
    var originalHeight = taskList.body.scrollHeight, decreasedHeight = 0;
    for( let selection of selectionList ) decreasedHeight += taskList.body.tr[ taskList.body.inputs.indexOf( selection ) ].offsetHeight;
    while( selectionList[0] )
			{ let selection = selectionList[0];
				removeTask( taskList.body.tr[ taskList.body.inputs.indexOf( selection ) ], taskList.body.labels[ taskList.body.inputs.indexOf( selection ) ], selection, taskList.body.boxes[ taskList.body.inputs.indexOf( selection ) ], taskList.body.texts[ taskList.body.inputs.indexOf( selection ) ] ) };
    setVerticalLayout( originalHeight, decreasedHeight ) };

const changeImportance = function( event, key, selectionList = [] ) // Alternar Importância da(s) Tarefa(s)
	{ if( ( event.type == "click" || event.type == "keypress" ) && !inputSelection.length ) return alertSelectionNeed();
		if( !selectionList.length ) for( let selection of inputSelection ) selectionList.push( selection.nextElementSibling );
		for( var selection of selectionList )
	 		{ if( selection.nextElementSibling.classList.contains( "finished" ) ) continue;
				for( var i = 1; i != 5; i++ )
			 		{ if( !selection.classList.contains( "imp" + i ) ) continue;
				 		var impNumber = i; break };
				selection.classList.remove( "imp" + impNumber );
				if( !taskList.foot.contains( event.target ) && ( key == "[" || key == "{" ) )
					{ selection.classList.add( "imp" + Math.min( 4, ( impNumber + 1 ) ) ) }
				else if( !taskList.foot.contains( event.target ) && ( key == "]" || key == "}" ) )
					{ selection.classList.add( "imp" + Math.max( 1, ( impNumber - 1 ) ) ) }
				else
					{ selection.classList.add( "imp" + ( impNumber >= 4 ? impNumber = 1 : ++impNumber ) ) } } };

const sortByImportance = function( order ) // Agrupar Tarefas por Importância
	{ var impClasses = [];
		for( let i = 4; i != 0; i-- )
			{ if( taskList.body.boxes.some( box => box.classList.contains( "imp" + i ) ) ) impClasses.push( "imp" + i ) };
		var targetRows = [];
		for( let imp of impClasses ) targetRows.push( taskList.body.tr.filter( row => row.oBringIt( taskList.body.boxes ).classList.contains( imp ) ) );
		targetRows.push( taskList.body.tr.filter( row => row.oBringIt( taskList.body.texts ).classList.contains( "finished" ) ) );
		order = order || ( targetRows[0].includes( taskList.body.firstElementChild ) ? "firstLessers" : "firstGreaters" );
	 	if( order == "firstLessers" ) targetRows.reverse();
		for( let i = 0; i < targetRows.length; i++ )
 			{ for( let row of targetRows[i] )
				{ row.value = row.oBringIt( taskList.body.inputs ).checked ? 1 : 0;
					if( row != targetRows[i][ targetRows[i].length - 1 ] ) continue;
					targetRows[i].sort( ( a, b ) => order == "firstLessers" ? a.value - b.value : b.value - a.value ) }
				for( let row of targetRows[i] )
					{ taskList.body.appendChild( row ); delete row.value } };
		focusTheExtremity( true ) };

const resetImportance = function( selectionList ) // Reconfigurar Importância de Tarefas
	{ for( var selection of selectionList )
	 		{ var actualBox = selection.previousElementSibling;
				if( selection.classList.contains( "finished" ) )
		 			{ for( var i = 1; i != 5; i++ )
							{ if( actualBox.classList.contains( "imp" + i ) )
									{ actualBox.classList.remove( "imp" + i ); break } } }
				else { actualBox.classList.add( "imp2" ) } } };
