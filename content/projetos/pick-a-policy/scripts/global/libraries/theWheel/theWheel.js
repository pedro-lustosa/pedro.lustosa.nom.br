"use strict";

/* Validações */
export const oPrimitiveToArray = function( evalued, primitive )
  { if( !Array.isArray( evalued ) ) evalued = typeof evalued == primitive ? Array.of( evalued ) : Array.from( evalued );
    return evalued }

export const oNodeToArray = function( evalued, shallow = true )
  { if( !Array.isArray( evalued ) ) evalued = evalued instanceof Node ? Array.of( evalued ) : Array.from( evalued );
    else if( shallow ) evalued = evalued.slice();
    return evalued }

/* Métodos de Textos */
numbers:
  { Object.defineProperty( String, "oNumbers", { value: [], enumerable: true } );
    for( let i = 48; i < 58; i++ ) String.oNumbers.push( String.fromCharCode( i ) ) }

Object.defineProperties( String.prototype,
  { oFindFirstChar: // Retorna o primeiro caractere de dado arranjo encontrado no alvo, ou o primeiro que não seja deste dado arranjo
      { value: function oFindFirstChar( charset, exclusive = false )
          { var actualChar = this.charAt( 0 ), includeExpression = exclusive ? "charset.includes( actualChar )" : "!charset.includes( actualChar )";
            for( let i = 1; actualChar && eval( includeExpression ); i++ ) actualChar = this.charAt( i );
            return actualChar },
        enumerable: true },
    oFindFirstIndex: // Retorna o número de índice do primeiro caractere de dado arranjo encontrado no alvo, ou do primeiro que não seja deste dado arranjo
      { value: function oFindFirstIndex( charset, exclusive = false )
          { return this.indexOf( this.oFindFirstChar( charset, exclusive ) ) },
        enumerable: true },
    oFilterNumber: // Retorna o primeiro número ou agrupamento numérico do alvo
      { value: function oFilterNumber()
          { var numberStart = this.slice( this.oFindFirstIndex( String.oNumbers ) );
            return Number( numberStart.slice( 0, numberStart.oFindFirstIndex( String.oNumbers, true ) ) ) },
        enumerable: true } } );

/* Métodos de Arranjos */
Object.defineProperties( Array.prototype,
  { oCycle: // Quando um número não for abarcado pelo índice de um arranjo, decompõe seu valor até que o seja
      { value: function oCycle( number )
          { if( number >= this.length )
              { number = number % this.length }
            else if( number < 0 )
              { while( number < 0 ) number += this.length };
            return this[ number ] },
        enumerable: true },
    oSelectForEach: // Itera um arranjo de elementos para retornar um arranjo com os elementos abarcados pela dada string de seleção
      { value: function oSelectForEach( selector, target = "children", count = 1 )
          { if( !this.every( element => element instanceof Element ) ) throw new RangeError( "Just arrays with only elements are allowed by oSelectForEach." );
            if( typeof selector != "string" ) throw new TypeError( "The selector argument of oSelectForEach must be a string." );
            if( count <= 0 ) throw new RangeError( "The count argument of oSelectForEach must be greater than 0." );
            var selection = [], method;
            switch( target )
              { case "children": method = count > 1 ? "querySelectorAll" : "querySelector"; break;
                case "parents": method = "closest"; break;
                default: throw new RangeError( "The allowed values for target argument of oSelectForEach are 'children' and 'parents'." ) };
            switch( method )
              { case "querySelector": this.forEach( element =>
                  { selection = selection.concat( element.querySelector( selector ) || [] ) } ); break;
                case "querySelectorAll": this.forEach( element =>
                  { selection = selection.concat( Array.from( element.querySelectorAll( selector ) ).slice( 0, count ) ) } ); break;
                case "closest": this.forEach( element =>
                  { for( let actualElement = element.parentElement, i = 0; ( actualElement = actualElement.closest( selector ) ) && i < count; i++ )
                      { selection.push( actualElement ); actualElement = actualElement.parentElement } } ) }
            return selection },
        enumerable: true } } );

/* Métodos de Nós */
Object.defineProperties( Node.prototype,
  { oChangeBySize: // Executa uma função ou retorna um boolean segundo o tamanho de dado objeto
      { value: function oChangeBySize( media, truthFunction, falsyFunction, evaluedNode )
          { var targetComponent;
            let acceptedMediaKeys = [ [ "innerWidth", "innerHeight", "outerWidth", "outerHeight" ],
                                      [ "width", "height", "availWidth", "availHeight" ],
                                      [ "clientWidth", "clientHeight", "offsetWidth", "offsetHeight", "scrollWidth", "scrollHeight" ] ];
            for( let size in media )
              { if( typeof media[ size ] !== "number" ) throw new TypeError( "Object value in media argument of oChangeBySize must be a number." );
                let sizeID;
                for( let i = 0; i < acceptedMediaKeys.length; i++ )
                  { if( acceptedMediaKeys[i].includes( size ) ) { sizeID = i; break } }
                switch( sizeID )
                  { case 0: targetComponent = window; break;
                    case 1: targetComponent = window.screen; break;
                    case 2: targetComponent = evaluedNode || this; break;
                    default: throw new RangeError( "Object key in media argument of oChangeBySize function must be a window, screen or element size property." ) }
                if( targetComponent[ size ] > media[ size ] ) return falsyFunction ? falsyFunction() : false }
            return truthFunction ? truthFunction() : true },
        enumerable: true },
    oChangePlacementBySize: // Realiza a inserção do alvo em um receptáculo em função do tamanho de dado objeto
      { value: function oChangePlacementBySize( media, receiver, nextSibling, evaluedNode )
          { let originalParent = this.originalParent = this.originalParent || this.parentElement,
                originalSibling = this.originalSibling = this.originalSibling || this.nextElementSibling || "none";
            if( this.oChangeBySize( media, false, false, evaluedNode || receiver ) )
              { if( !Array.from( receiver.children ).includes( this ) ) nextSibling ? receiver.insertBefore( this, nextSibling ) : receiver.appendChild( this ) }
            else
              { if( Array.from( receiver.children ).includes( this ) ) originalSibling != "none" ? originalParent.insertBefore( this, originalSibling ) : originalParent.appendChild( this ) } },
        enumerable: true } } );

/* Métodos de Elementos */
Object.defineProperties( Element.prototype,
  { oFindDepth: // Retorna um número correspondente à profundidade do alvo na árvore do DOM
      { value: function oFindDepth( baseElement = document.documentElement, depth = 0 )
          { if( !baseElement.contains( this ) ) return;
            if( baseElement.isSameNode( this ) ) return depth;
            var elementChildren = baseElement.children;
            for( let child of elementChildren )
              { if( child.contains( this ) ) return this.oFindDepth( child, ++depth ) } },
        enumerable: true },
    oBringParents: // Retorna um arranjo com os elementos do primeiro argumento ascendentes do alvo, opcionalmente delimitados por quantidade e alcance
      { value: function oBringParents( elementList, count = Infinity, range = Infinity )
          { var actualParent = this.parentElement, targetParents = [];
            if( count <= 0 ) return [];
            if( range < 0 ) if( ( range = this.oFindDepth() + range ) < 0 ) return count == 1 ? undefined : [];
            var arrayList = oNodeToArray( elementList );
            parentsLoop: while( actualParent && arrayList.length && range )
              { while( arrayList.includes( actualParent ) )
                  { targetParents = targetParents.concat( arrayList.splice( arrayList.findIndex( element => element.isSameNode( actualParent ) ), 1 ) );
                    if( targetParents.length == count ) break parentsLoop }
                actualParent = actualParent.parentElement; range-- }
            return count == 1 ? targetParents.pop() : targetParents },
        enumerable: true },
    oBringChildren: // Retorna um arranjo com os elementos do primeiro argumento descendentes do alvo, opcionalmente delimitados por quantidade e alcance
      { value: function oBringChildren( elementList, count = Infinity, range = Infinity )
          { var actualChildren = this.children, targetChildren = [];
            if( !actualChildren.length || count <= 0 || range <= 0 ) return [];
            var arrayList = oNodeToArray( elementList );
            childrenLoop: for( let child of actualChildren )
              { while( arrayList.includes( child ) )
                  { targetChildren = targetChildren.concat( arrayList.splice( arrayList.findIndex( element => element.isSameNode( child ) ), 1 ) );
                    if( targetChildren.length == count ) break childrenLoop }
                if( range > 1 && arrayList.length ) targetChildren = targetChildren.concat( child.oBringChildren( arrayList, count - targetChildren.length, range - 1 ) || [] ) }
            return count == 1 ? targetChildren.pop() : targetChildren },
        enumerable: true },
    oTriggerMediaLoading: // Habilita a possibilidade de carregamento de dado recurso midiático
      { value: function oTriggerMediaLoading()
          { switch( this.tagName )
              { case "PICTURE": for( let image of this.children ) checkDataset( image ); break;
                case "SOURCE":
                case "IMG": checkDataset( this ); break;
                case "VIDEO":
                case "AUDIO": checkDataset( this );
                  for( let source of this.children ) checkDataset( source ); break;
                default: throw new RangeError( "The target of oTriggerMediaLoading must be either a <picture>, <source>, <img>, <video> or <audio> element." ) }
            function checkDataset( element )
              { if( element.dataset.src )
                  { element.setAttribute( element.tagName == "SOURCE" && element.parentElement.tagName == "PICTURE" ? "srcset" : "src", element.dataset.src );
                    element.removeAttribute( "data-src" ) } } },
        enumerable: true },
    oChangeClassesBySize: // Alterna as classes do alvo em função do tamanho de tela
      { value: function oChangeClassesBySize( media, classes, evaluedNode )
          { for( let operator in classes )
              { classes[ operator ] = oPrimitiveToArray( classes[ operator ], "string" );
                switch( operator )
                  { case "add": for( let _class of classes[ operator ] ) this.classList.toggle( _class, this.oChangeBySize( media, false, false, evaluedNode ) ); break;
                    case "remove": for( let _class of classes[ operator ] ) this.classList.toggle( _class, !this.oChangeBySize( media, false, false, evaluedNode ) ); break;
                    default: throw new RangeError( "Object key in classes argument of oChangeClassesBySize function must be 'add' or 'remove'." ) } } },
        enumerable: true } } );

/* Métodos de Elementos do HTML */
Object.defineProperties( HTMLElement.prototype,
  { oMatchSize: // Redimensiona alvo em relação a dado elemento
      { value: function oMatchSize( element = this.previousElementSibling || this.nextElementSibling, targetSizes = { width: "scrollWidth", height: "scrollHeight" }, multiplier = 1 )
          { if( !( element instanceof HTMLElement ) ) throw new TypeError( "The element argument of oMatchSize must be a HTMLElement." );
            var allowedKeys = [ "minWidth", "minHeight", "width", "height", "maxWidth", "maxHeight" ], allowedValues = [ "clientWidth", "clientHeight", "offsetWidth", "offsetHeight", "scrollWidth", "scrollHeight" ];
            for( let size in targetSizes )
              { if( !allowedKeys.includes( size ) ) throw new RangeError( "Object keys in targetSizes argument of oMatchSize must be valid CSS properties related to width or height." );
                if( !allowedValues.includes( targetSizes[ size ] ) ) throw new RangeError( "Object value in targetSizes argument of oMatchSize must be an DOM element size property." );
                this.style[ size ] = element[ targetSizes[ size ] ] * multiplier + "px" } },
        enumerable: true },
    oFlatSize: // Equaliza medidas do alvo às de dado elemento
      { value: function oFlatSize( element = this.previousElementSibling || this.nextElementSibling, width = true, height = true )
          { if( width && height ) return this.oMatchSize( element, { width: "offsetWidth", height: "offsetHeight" }, 1 );
            if( width ) return this.oMatchSize( element, { width: "offsetWidth" }, 1 );
            if( height ) return this.oMatchSize( element, { height: "offsetHeight" }, 1 );
            throw new TypeError( "The function oFlatSize requires that at least one size measure be passed." ) },
        enumerable: true },
    oEncompassChildren: // Expande medidas do alvo até que alcance a total de seus descendentes
      { value: function oEncompassChildren( width = true, height = true )
          { for( let size of arguments )
              { if( typeof size !== "boolean" ) throw new TypeError( "oEncompassChildren arguments must be booleans; the first, to enable width resizing, the second, to enable height resizing." ) };
            var boxType = window.getComputedStyle( this ).boxSizing;
            if( width ) changeSize.call( this, "width" ); if( height ) changeSize.call( this, "height" );
            function changeSize( size )
              { var scrollSize = "scroll" + ( size[ 0 ].toUpperCase() + size.slice( 1 ) );
                this.style[ size ] = this[ scrollSize ] + "px" } },
        enumerable: true },
    oChangeText: // Altera Texto do Alvo, e Armazena o Original em uma Propriedade
      { value: function oChangeText( textSource, textType = "textContent", update = false )
          { if( this[ textType ] == textSource ) return;
            if( typeof textSource !== "string" ) throw new TypeError( "Argument textSource of oChangeText must be a string." );
            let allowedProperties = [ "innerText", "outerText", "textContent", "innerHTML" ];
            if( !allowedProperties.includes( textType ) ) throw new RangeError( "Object value for textType argument of the function oChangeText must be an element text property." );
            if( !this[ 'formerText' ] || update ) this.formerText = this[ textType ];
            this[ textType ] = textSource },
        enumerable: true } } );
