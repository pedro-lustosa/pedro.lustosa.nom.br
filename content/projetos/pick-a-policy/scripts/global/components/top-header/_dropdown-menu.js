/* Identificadores */

const topHeader = document.getElementById( "top-header" ); // Cabeçalho Superior da Página
topHeader:
  { var dropdownMenu = topHeader.dropdownMenu = topHeader.querySelector( ".dropdown-menu" ) || topHeader.querySelector( ".nav-container" ); /* Menu Deslizante do Layout Responsivo */
    dropdownMenu:
      { var pagesList = dropdownMenu.list = dropdownMenu.querySelector( ".pages-list" ) } }

/* Ouvintes de Evento */

toggleDropdownMenu: // Expandir e Retrair Menu Responsivo do Cabeçalho Superior
  { dropdownMenu.addEventListener( "click", () =>
      { if( window.innerWidth >= 500 ) return;
        pagesList.offsetHeight ? pagesList.style.height = "0px" : pagesList.oEncompassChildren() } )
    dropdownMenu.addEventListener( "blur", () =>
      { if( window.innerWidth >= 500 ) return;
        pagesList.style.height = "0px" } ) }
