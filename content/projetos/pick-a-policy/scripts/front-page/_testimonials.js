/* Identificadores */

const testimonials = document.getElementById( "testimonials" ); // Seção de Depoimentos
testimonials:
  { let slideshow = testimonials.slideshow = testimonials.querySelector( ".slideshow-content" ); /* Slideshow com Depoimentos */
    slideshow:
      { let images = slideshow.images = Array.from( testimonials.getElementsByClassName( "slideshow-image" ) ); /* Imagens do SlideShow */
        images:
          { images.displayedImage = images.find( image => image.offsetHeight ) /* Imagem do Slideshow no Momento Exibida */ }
        let texts = slideshow.texts = Array.from( testimonials.getElementsByClassName( "slideshow-text" ) ); /* Textos do Slideshow */
        texts:
          { texts.displayedText = texts.find( text => text.offsetHeight ) /* Texto do Slideshow no Momento Exibido */ }
        slideshow.container = texts.displayedText.parentElement }
    let arrows = testimonials.arrowsSet = testimonials.querySelector( ".arrows-set" ); // Setas de Navegação pelo SlideShow
    arrows:
      { arrows.left = arrows.querySelector( ".larr" ); arrows.right = arrows.querySelector( ".rarr" ) /* Setas Esquerda e Direita */ } }

/* Ouvintes de Evento */

slideshowSetup: // Funcionalidades do Slideshow
  { let slideshow = testimonials.slideshow, images = slideshow.images, texts = slideshow.texts, container = slideshow.container,
        arrows = testimonials.arrowsSet;
    changeArrowsPlacement: // Em Telas de Pouca Largura, Deslocar Setas para Baixo do Slide
      { for( let _event of [ "load", "resize" ] )
          { window.addEventListener( _event, () => arrows.oChangePlacementBySize( { innerWidth: 1129 }, container, texts.displayedText ) ) } }
    changeSlideshowContent: // Alterar Conteúdo Atual Exibido pelo Slideshow
      { let arrowsOperands = { left: -1, right: 1 };
        for( let arrow in arrowsOperands )
          { arrows[ arrow ].addEventListener( "click", () =>
              changeSlideContent( container, images.oCycle( images.indexOf( images.displayedImage ) + arrowsOperands[ arrow ] ), texts.oCycle( texts.indexOf( texts.displayedText ) + arrowsOperands[ arrow ] ) ) ) } } }

/* Funções Específicas */

const changeSlideContent = function( baseParent, newImage, newText ) // Inserir Imagem e Texto do Conteúdo Oculto do Slideshow na Página, e Vice-Versa
  { let displayedImage = testimonials.slideshow.images.displayedImage, displayedText = testimonials.slideshow.texts.displayedText,
        arrowsSet = testimonials.arrowsSet, swapSets = [ [ newImage, displayedImage ], [ newText, displayedText ] ];
    newImage.oTriggerMediaLoading();
    for( let set of swapSets ) set[ 0 ].replaceWith( set[ 1 ] );
    if( Array.from( baseParent.children ).includes( arrowsSet ) )
      { arrowsSet.before( newImage ); arrowsSet.after( newText ) }
    else
      { baseParent.append( newImage, newText ) };
    updateDisplayedContent( newImage, newText ) }

const updateDisplayedContent = function( newImage, newText ) // Atualizar Atuais Imagem e Texto sendo Exibidos
  { testimonials.slideshow.images.displayedImage = newImage; testimonials.slideshow.texts.displayedText = newText }
