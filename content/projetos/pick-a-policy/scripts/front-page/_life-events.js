/* Identificadores */

const lifeEvents = document.getElementById( "life-events" ); // Seção de Ilustrações sobre Benefícios de Seguros
lifeEvents:
  { var imagesGroup = lifeEvents.imagesGroup = lifeEvents.querySelector( ".side-images" ); /* Grupo com as Ilustrações */
    imagesGroup:
      { var imagesContainers = imagesGroup.containers = Array.from( imagesGroup.getElementsByClassName( "image-container" ) ); /* Recipientes das Imagens e de suas Descrições */
        var imagesRows = imagesGroup.rows = imagesGroup.getElementsByClassName( "row" ) /* Fileiras para Inserção de Imagens */ } }

/* Ouvintes de Evento */

changeLifeEventsImagesPlacement: // Alterar o Posicionamento de Imagens da Seção de Ilustrações Sobre Benefícios de Seguros
  { let images = imagesContainers.slice().reverse();
    for( let image of images )
      { for( let _event of [ "load", "resize" ] )
          { window.addEventListener( _event, () => image.oChangePlacementBySize( { innerWidth: 1579 }, imagesGroup ) ) } } }
