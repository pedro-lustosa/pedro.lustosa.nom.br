"use strict";

/* Livrarias */

import "../global/libraries/theWheel/theWheel-min.js"; // The Wheel

/* Componentes */

// Menu Deslizante
( function()
    { const topHeader = document.getElementById( "top-header" ); // Cabeçalho Superior da Página
      topHeader:
        { var dropdownMenu = topHeader.dropdownMenu = topHeader.querySelector( ".dropdown-menu" ) || topHeader.querySelector( ".nav-container" ); /* Menu Deslizante do Layout Responsivo */
          dropdownMenu:
            { var pagesList = dropdownMenu.list = dropdownMenu.querySelector( ".pages-list" ) } }

      /* Ouvintes de Evento */

      toggleDropdownMenu: // Expandir e Retrair Menu Responsivo do Cabeçalho Superior
        { dropdownMenu.addEventListener( "click", () =>
            { if( window.innerWidth >= 500 ) return;
              pagesList.offsetHeight ? pagesList.style.height = "0px" : pagesList.oEncompassChildren() } )
          dropdownMenu.addEventListener( "blur", () =>
            { if( window.innerWidth >= 500 ) return;
              pagesList.style.height = "0px" } ) } } )();

// Banner de Logomarcas da Empresa
( function()
    { /* Identificadores */

      const intro = document.getElementById( "intro" ); // Seção de Abertura da Página
      intro:
        { var footerBlock = intro.footerBlock = intro.querySelector( ".footer-block" ); /* Subseção que Exibe Empresas Relacionadas a Pick a Policy */
          footerBlock:
            { var companiesList = footerBlock.companiesList = footerBlock.querySelector( ".companies-list" ), /* Lista de Empresas Relacionadas a Pick a Policy */
                  displayerButton = footerBlock.displayerButton = footerBlock.querySelector( ".displayer-button" ); /* Botão para Expandir Lista de Empresas */
              displayerButton:
                { var buttonBody = displayerButton.body = displayerButton.querySelector( ".button-body" ) /* Parte Central do Botão, Clicável */ } } }

      /* Ouvintes de Evento */

      expandCompaniesList: // Expandir Lista de Empresas da Seção de Introdução
        { for( let _event of [ "load", "resize" ] )
            { window.addEventListener( _event, () => companiesList.oChangeBySize( { innerWidth: 1099 },
                companiesList.oMatchSize.bind( companiesList, companiesList.firstElementChild, { height: "offsetHeight" }, 2 ), companiesList.oFlatSize.bind( companiesList, companiesList.firstElementChild, false ) ) ) };
          buttonBody.addEventListener( "click", () =>
            { if( window.innerWidth >= 1100 )
                { buttonBody.classList.toggle( "x" ) ? companiesList.oEncompassChildren() : companiesList.oFlatSize( companiesList.firstElementChild, false ) }
              else
                { let childrenSet = Array.from( companiesList.children ).slice( 0, 2 );
                  for( let child of childrenSet ) companiesList.append( child ) } } ) }

      revalueLists: // Agrupar em <div>'s Itens da Lista de Empresas por Fileira
        { window.addEventListener( "resize", () => revalueGroupsNumber() ) }

      changeButtonImage: // Altera a Imagem do Botão de Exibição das Logomarcas
        { window.addEventListener( "resize", () => { buttonBody.oChangeClassesBySize( { innerWidth: 1099 }, { add: "arrow-circle", remove: "plus-circle" } ) } ) }

      /* Funções Específicas */

      const findGroupItensNumber = function() // Encontra por Tamanho de Tela a Quantidade de <li>'s da Lista de Empresas Agrupada por <div>'s
        { var viewportWidth = window.innerWidth, groupItensNumber;
          switch( true )
            { case viewportWidth >= 950: groupItensNumber = 5; break;
              case viewportWidth < 950 && viewportWidth >= 800: groupItensNumber = 4; break;
              case viewportWidth < 800 && viewportWidth >= 600: groupItensNumber = 3; break;
              case viewportWidth < 600 && viewportWidth >= 450: groupItensNumber = 2; break;
              default: groupItensNumber = 1 }
          return groupItensNumber }

      const agroupLists = function( groupItensNumber ) // Agrupar em <div>'s Itens da Lista de Empresas por Fileira
        { groupItensNumber = groupItensNumber || findGroupItensNumber();
          let listItens = companiesList.oBringChildren( companiesList.querySelectorAll( "li" ), undefined, 1 ),
              listItensNumber = listItens.length, listItensValues = listItens.values();
          for( let i = listItensNumber % groupItensNumber == 0 ? Math.trunc( listItensNumber/groupItensNumber ) : Math.trunc( listItensNumber/groupItensNumber ) + 1; i; i-- )
            { let li = companiesList.appendChild( document.createElement( "LI" ) ), ul = li.appendChild( document.createElement( "UL" ) );
              li.classList.add( "list-container" );
              for( let i = groupItensNumber; i; i-- )
                { let item = listItensValues.next().value; if( !item ) break;
                  ul.appendChild( item ) } } }

      const revalueGroupsNumber = function() // Avaliar se o Número de <div>'s Agupadoras dos Itens da Lista de Empresas precisa ser Redefinido
        { var targetNumber = findGroupItensNumber(), actualNumber = companiesList.childElementCount;
          if( targetNumber == actualNumber ) return;
          while( companiesList.firstElementChild.classList.contains( "list-container" ) )
            { let actualContainer = companiesList.firstElementChild,
                  containerItens = actualContainer.oBringChildren( actualContainer.querySelectorAll( "li" ), undefined, 2 );
              for( let item of containerItens ) companiesList.appendChild( item );
              actualContainer.remove() }
          return agroupLists( targetNumber ) }

      /* Instruções Iniciais */

      buttonBody.oChangeClassesBySize( { innerWidth: 1099 }, { add: "arrow-circle", remove: "plus-circle" } ); agroupLists() } )();

// Seção de Ilustrações sobre Benefícios de Seguros
( function()
    { /* Identificadores */
      const lifeEvents = document.getElementById( "life-events" ); // Seção de Ilustrações sobre Benefícios de Seguros
      lifeEvents:
        { var imagesGroup = lifeEvents.imagesGroup = lifeEvents.querySelector( ".side-images" ); /* Grupo com as Ilustrações */
          imagesGroup:
            { var imagesContainers = imagesGroup.containers = Array.from( imagesGroup.getElementsByClassName( "image-container" ) ); /* Recipientes das Imagens e de suas Descrições */
              var imagesRows = imagesGroup.rows = imagesGroup.getElementsByClassName( "row" ) /* Fileiras para Inserção de Imagens */ } }

      /* Ouvintes de Evento */

      changeLifeEventsImagesPlacement: // Alterar o Posicionamento de Imagens da Seção de Ilustrações Sobre Benefícios de Seguros
        { let images = imagesContainers.slice().reverse();
          for( let image of images )
            { for( let _event of [ "load", "resize" ] )
                { window.addEventListener( _event, () => image.oChangePlacementBySize( { innerWidth: 1579 }, imagesGroup ) ) } } } } )();

// Banner de Histórico
( function()
    { /* Identificadores */

    const trackBanner = document.getElementById( "track-banner" ); // Banner de Histórico
    trackBanner:
      { var textContainer = trackBanner.textContainer = trackBanner.querySelector( ".text-container" ); /* Recipiente de Texto do Banner */
        textContainer:
          { var textSet = textContainer.textSet = textContainer.querySelector( ".text-set" ) /* Conjunto de Textos do Banner */ }
        var displayText = trackBanner.displayText = trackBanner.querySelector( ".side-image" ) /* Texto Decorativo a Originalmente Aparecer ao Lado do Recipiente de Texto */ }

    /* Ouvintes de Evento */

    changeTrackBannerArranging: // Alterar Posicionamento da Imagem Lateral do Banner de Histórico para Dentro do Conteúdo Textual
      { for( let _event of [ "load", "resize" ] ) window.addEventListener( _event, () => displayText.oChangePlacementBySize( { innerWidth: 999 }, textContainer, textSet ) ) } } )();

// Seção de Depoimentos
( function()
    { /* Identificadores */

      const testimonials = document.getElementById( "testimonials" ); // Seção de Depoimentos
      testimonials:
        { let slideshow = testimonials.slideshow = testimonials.querySelector( ".slideshow-content" ); /* Slideshow com Depoimentos */
          slideshow:
            { let images = slideshow.images = Array.from( testimonials.getElementsByClassName( "slideshow-image" ) ); /* Imagens do SlideShow */
              images:
                { images.displayedImage = images.find( image => image.offsetHeight ) /* Imagem do Slideshow no Momento Exibida */ }
              let texts = slideshow.texts = Array.from( testimonials.getElementsByClassName( "slideshow-text" ) ); /* Textos do Slideshow */
              texts:
                { texts.displayedText = texts.find( text => text.offsetHeight ) /* Texto do Slideshow no Momento Exibido */ }
              slideshow.container = texts.displayedText.parentElement }
          let arrows = testimonials.arrowsSet = testimonials.querySelector( ".arrows-set" ); // Setas de Navegação pelo SlideShow
          arrows:
            { arrows.left = arrows.querySelector( ".larr" ); arrows.right = arrows.querySelector( ".rarr" ) /* Setas Esquerda e Direita */ } }

      /* Ouvintes de Evento */

      slideshowSetup: // Funcionalidades do Slideshow
        { let slideshow = testimonials.slideshow, images = slideshow.images, texts = slideshow.texts, container = slideshow.container,
              arrows = testimonials.arrowsSet;
          changeArrowsPlacement: // Em Telas de Pouca Largura, Deslocar Setas para Baixo do Slide
            { for( let _event of [ "load", "resize" ] )
                { window.addEventListener( _event, () => arrows.oChangePlacementBySize( { innerWidth: 1129 }, container, texts.displayedText ) ) } }
          changeSlideshowContent: // Alterar Conteúdo Atual Exibido pelo Slideshow
            { let arrowsOperands = { left: -1, right: 1 };
              for( let arrow in arrowsOperands )
                { arrows[ arrow ].addEventListener( "click", () =>
                    changeSlideContent( container, images.oCycle( images.indexOf( images.displayedImage ) + arrowsOperands[ arrow ] ), texts.oCycle( texts.indexOf( texts.displayedText ) + arrowsOperands[ arrow ] ) ) ) } } }

      /* Funções Específicas */

      const changeSlideContent = function( baseParent, newImage, newText ) // Inserir Imagem e Texto do Conteúdo Oculto do Slideshow na Página, e Vice-Versa
        { let displayedImage = testimonials.slideshow.images.displayedImage, displayedText = testimonials.slideshow.texts.displayedText,
              arrowsSet = testimonials.arrowsSet, swapSets = [ [ newImage, displayedImage ], [ newText, displayedText ] ];
          newImage.oTriggerMediaLoading();
          for( let set of swapSets ) set[ 0 ].replaceWith( set[ 1 ] );
          if( Array.from( baseParent.children ).includes( arrowsSet ) )
            { arrowsSet.before( newImage ); arrowsSet.after( newText ) }
          else
            { baseParent.append( newImage, newText ) };
          updateDisplayedContent( newImage, newText ) }

      const updateDisplayedContent = function( newImage, newText ) // Atualizar Atuais Imagem e Texto sendo Exibidos
        { testimonials.slideshow.images.displayedImage = newImage; testimonials.slideshow.texts.displayedText = newText } } )();

// Banner com Formulário de Estimativa de Preço
( function()
    { /* Identificadores */

    const quoteBanner = document.getElementById( "quote-banner" ); // Banner com Formulário de Estimativa de Preço
    quoteBanner:
      { var heading = quoteBanner.heading = quoteBanner.querySelector( ".title" ); /* Título do Banner */
        heading:
          { var mainLine = heading.mainLine = heading.querySelector( ".main-line" ), /* Linha Principal do Título */
                sideLine = heading.sideLine = heading.querySelector( ".side-line" ) /* Linha Periférica do Título */ } }

    /* Ouvintes de Evento */

    changeQuoteBannerText: // Alterar Texto do Banner com Formulário de Estimativa de Preço
      { for( let _event of [ "load", "resize" ] ) window.addEventListener( _event, () => mainLine.oChangeBySize( { innerWidth: 639 },
          mainLine.oChangeText.bind( mainLine, mainLine.dataset.text ), mainLine.oChangeText.bind( mainLine, mainLine.formerText || mainLine.textContent ) ) ) } } )()
