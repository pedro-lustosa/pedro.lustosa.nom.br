"use strict";

/* Validações */
const oPrimitiveToArray = function( evalued, primitive )
  { if( !Array.isArray( evalued ) ) evalued = typeof evalued == primitive ? Array.of( evalued ) : Array.from( evalued );
    return evalued }

const oNodeToArray = function( evalued )
  { if( !Array.isArray( evalued ) ) evalued = evalued.nodeType ? Array.of( evalued ) : Array.from( evalued );
    return evalued }

/* Métodos de Arranjos */
Object.defineProperties( Array.prototype,
  { oSingle: // Elimina valores repetidos do arranjo
      { value: function oSingle()
          { return this.filter( ( object, index, array ) => array.indexOf( object ) == index ) } },
    oTrim: // Elimina valores de um arranjo até o primeiro que atenda à avaliação da função de parâmetro
      { value: function oTrim( evaluer )
          { return this.slice( this.findIndex( value => evaluer( value ) ) || this.length ) } },
    oSegment: // Divide o arranjo em sub-arranjos, a começarem com o valor que atender à avaliação da função de parâmetro
      { value: function oSegment( evaluer, trim = false )
          { var newArray = [], startIndex = 0;
            this.forEach( ( value, index, array ) =>
              { if( index && evaluer( value ) ) { newArray.push( array.slice( startIndex, index ) ); startIndex = index } } );
            newArray.push( this.slice( startIndex ) );
            if( trim && !evaluer( newArray[0][0] ) ) newArray.shift();
            return newArray } },
    oSegmentByEnd: // Divide o arranjo em sub-arranjos, a terminarem com o valor que atender à avaliação da função de parâmetro
      { value: function oSegmentByEnd( evaluer, trim = false )
          { var newArray = [], startIndex = 0;
            this.forEach( ( value, index, array ) =>
              { if( evaluer( value ) ) { newArray.push( array.slice( startIndex, index + 1 ) ); startIndex = index + 1 } } );
            if( !trim && startIndex != this.length ) newArray.push( this.slice( startIndex ) );
            return newArray } },
    oCycle: // Quando um número não for abarcado pelo índice de um arranjo, decompõe seu valor até que o seja
      { value: function oCycle( number )
          { if( number >= this.length )
              { number = number % this.length }
            else if( number < 0 )
              { while( number < 0 ) number += this.length };
            return this[ number ] } } } );

/* Métodos de Eventos */
Object.defineProperties( EventTarget.prototype,
  { oAssignEvents: // Definir Valores de Propriedades de Eventos
      { value: function oAssignEvents( events )
        { for( let event in events ) this[ "on" + event ] = events[ event ] } },
    oCatchClicksWithKeys: // Acionar Cliques através de Teclagens
      { value: function oCatchClicksWithKeys( event, keys = [ "Enter" ] )
        { if( keys.some( key => event.key == key ) ) this.click() } } } );

/* Métodos de Nós */
Object.defineProperties( Node.prototype,
  { oChangeBySize: // Executa uma função ou retorna um boolean segundo o tamanho de dado objeto
      { value: function oChangeBySize( media, truthFunction, falsyFunction, evaluedNode )
          { var targetComponent;
            let acceptedMediaKeys = [ [ "innerWidth", "innerHeight", "outerWidth", "outerHeight" ],
                                      [ "width", "height", "availWidth", "availHeight" ],
                                      [ "clientWidth", "clientHeight", "offsetWidth", "offsetHeight", "scrollWidth", "scrollHeight" ] ];
            for( let size in media )
              { if( typeof media[ size ] !== "number" ) throw new TypeError( "Object value in media argument of oChangeBySize must be a number." );
                let sizeID;
                for( let i = 0; i < acceptedMediaKeys.length; i++ )
                  { if( acceptedMediaKeys[i].includes( size ) ) { sizeID = i; break } }
                switch( sizeID )
                  { case 0: targetComponent = window; break;
                    case 1: targetComponent = window.screen; break;
                    case 2: targetComponent = evaluedNode || this; break;
                    default: throw new RangeError( "Object key in media argument of oChangeBySize function must be a window, screen or element size property." ) }
                if( targetComponent[ size ] > media[ size ] ) return falsyFunction ? falsyFunction() : false }
            return truthFunction ? truthFunction() : true } },
    oChangePlacementBySize: // Realiza a inserção do alvo em um receptáculo em função do tamanho de dado objeto
      { value: function oChangePlacementBySize( media, receiver, nextSibling, evaluedNode )
          { let originalParent = this.originalParent = this.originalParent || this.parentElement,
                originalSibling = this.originalSibling = this.originalSibling || this.nextElementSibling || "none";
            if( this.oChangeBySize( media, false, false, evaluedNode || receiver ) )
              { if( !Array.from( receiver.children ).includes( this ) ) nextSibling ? receiver.insertBefore( this, nextSibling ) : receiver.appendChild( this ) }
            else
              { if( Array.from( receiver.children ).includes( this ) ) originalSibling != "none" ? originalParent.insertBefore( this, originalSibling ) : originalParent.appendChild( this ) } } } } );

/* Métodos de Elementos */
Object.defineProperties( Element.prototype,
  { oFindDepth: // Retorna um número correspondente à profundidade do alvo na árvore do DOM
      { value: function oFindDepth( baseElement = document.documentElement, depth = 0 )
        { if( !baseElement.contains( this ) ) return;
          if( baseElement.isSameNode( this ) ) return depth;
          var elementChildren = baseElement.children;
          for( let child of elementChildren )
            { if( child.contains( this ) ) return this.oFindDepth( child, ++depth ) } } },
    oBringParents:
      { value: function oBringParents( elementList, count = Infinity, range = Infinity )
        { var actualParent = this.parentElement, targetParents = [];
          if( count <= 0 ) return [];
          if( range < 0 ) if( ( range = this.oFindDepth() + range ) < 0 ) return count == 1 ? undefined : [];
          elementList = oNodeToArray( elementList );
          parentsLoop: while( actualParent && elementList.length && range )
            { while( elementList.includes( actualParent ) )
                { targetParents = targetParents.concat( elementList.splice( elementList.findIndex( element => element.isSameNode( actualParent ) ), 1 ) );
                  if( targetParents.length == count ) break parentsLoop }
              actualParent = actualParent.parentElement; range-- }
          return count == 1 ? targetParents.pop() : targetParents } },
    oBringChildren: // Retorna um arranjo com os elementos do primeiro argumento descendentes do alvo, opcionalmente delimitados por quantidade e alcance
      { value: function oBringChildren( elementList, count = Infinity, range = Infinity )
          { var actualChildren = this.children, targetChildren = [];
            if( !actualChildren.length || count <= 0 || range <= 0 ) return [];
            elementList = oNodeToArray( elementList );
            childrenLoop: for( let child of actualChildren )
              { while( elementList.includes( child ) )
                  { targetChildren = targetChildren.concat( elementList.splice( elementList.findIndex( element => element.isSameNode( child ) ), 1 ) );
                    if( targetChildren.length == count ) break childrenLoop }
                if( range > 1 && elementList.length ) targetChildren = targetChildren.concat( child.oBringChildren( elementList, count - targetChildren.length, range - 1 ) || [] ) }
            return count == 1 ? targetChildren.pop() : targetChildren } },
    oChangeClassesBySize: // Alterna as Classes do Alvo em Função do Tamanho de Tela
      { value: function oChangeClassesBySize( media, classes, evaluedNode )
          { for( let operator in classes )
              { classes[ operator ] = oPrimitiveToArray( classes[ operator ], "string" );
                switch( operator )
                  { case "add": for( let _class of classes[ operator ] ) this.classList.toggle( _class, this.oChangeBySize( media, false, false, evaluedNode ) ); break;
                    case "remove": for( let _class of classes[ operator ] ) this.classList.toggle( _class, !this.oChangeBySize( media, false, false, evaluedNode ) ); break;
                    default: throw new RangeError( "Object key in classes argument of oChangeClassesBySize function must be 'add' or 'remove'." ) } } } } } );

/* Métodos de Elementos do HTML */
Object.defineProperties( HTMLElement.prototype,
  { oFlatSize: // Equaliza Medidas do Alvo às do Agente
      { value: function oFlatSize( element = this.firstElementChild, targetSizes = { height: "offsetHeight" }, multiplier = 1 )
          { for( let size in targetSizes )
              { if( ![ "width", "height" ].includes( size ) ) throw new RangeError( "Object key in targetSizes argument of oFlatSize function must be 'width' or 'height'." )
                let allowedSizes = [ "clientWidth", "clientHeight", "offsetWidth", "offsetHeight", "scrollWidth", "scrollHeight" ];
                if( !allowedSizes.includes( targetSizes[ size ] ) ) throw new RangeError( "Object value in targetSizes argument of oFlatSize function must be an element size property." );
                this.style[ size ] = element[ targetSizes[ size ] ] * multiplier + "px" } } },
    oConstraintSize: // Limita Medidas do Alvo às do Agente
        { value: function oConstraintSize( element = this.firstElementChild, targetSizes = { height: "offsetHeight" }, multiplier = 1 )
            { for( let size in targetSizes )
                { if( ![ "width", "height" ].includes( size ) ) throw new RangeError( "Object key in targetSizes argument of oConstraintSize function must be 'width' or 'height'." )
                  let allowedSizes = [ "clientWidth", "clientHeight", "offsetWidth", "offsetHeight", "scrollWidth", "scrollHeight" ];
                  if( !allowedSizes.includes( targetSizes[ size ] ) ) throw new RangeError( "Object value in targetSizes argument of oConstraintSize function must be an element size property." );
                  this.style[ "max-" + size ] = element[ targetSizes[ size ] ] * multiplier + "px" } } },
    oEncompassChildren: // Expande Medidas do Alvo até que Alcance a Total de seus Descendentes
      { value: function oEncompassChildren( width = true, height = true )
          { if( width ) this.style.width = this.scrollWidth + "px";
            if( height ) this.style.height = this.scrollHeight + "px" } },
    oChangeText: // Altera Texto do Alvo, e Armazena o Original em uma Propriedade
      { value: function oChangeText( textSource, textType = "textContent", update = false )
          { if( this[ textType ] == textSource ) return;
            if( typeof textSource !== "string" ) throw new TypeError( "Argument textSource of oChangeText must be a string." );
            let allowedProperties = [ "innerText", "outerText", "textContent", "innerHTML" ];
            if( !allowedProperties.includes( textType ) ) throw new RangeError( "Object value for textType argument of the function oChangeText must be an element text property." );
            if( !this[ 'formerText' ] || update ) this.formerText = this[ textType ];
            this[ textType ] = textSource } } } );

/* Métodos de Elementos do HTML */
	/* DL */
  Object.defineProperties( HTMLDListElement.prototype,
    { oGetTerms:
        { value: function getTerms() // Retorna todos os DT diretos da DL
            { return Array.from( this.children ).filter( element => element.tagName == "DT" ) } },
      oGetDescriptions:
        { value: function getDescriptions() // Retorna todos os DD diretos da DL
            { return Array.from( this.children ).filter( element => element.tagName == "DD" ) } },
      oGetEntries:
        { value: function getEntries() // Retorna arranjos encabeçados pelos DT diretos da DL e continuados por seus respectivos DD
            { var dt = this.oGetTerms(), dlEntries = [];
              for( let i = 0; i < dt.length; i++ )
                { dlEntries.push( Array.of( dt[i] ) );
                  var targetElement = dlEntries[i][0].nextElementSibling;
                  while( targetElement && targetElement.tagName != "DT" )
                    { if( targetElement.tagName == "DD" ) dlEntries[i].push( targetElement );
                      targetElement = targetElement.nextElementSibling } };
              return dlEntries } } } );
